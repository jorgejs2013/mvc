<!DOCTYPE html>
<html lang="pt-BR">
<head>
<title>Loja virtual admin</title>
<meta charset="utf-8" />

<meta http-equiv="X-UA-Compatible" content="IE-edge">
<meta name="robots" content="noindex, nofollow"> 

<link href="<?php echo BASE_URL;?>assets/css/dash.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />

<?php
  require_once("Views/arquivos_css.php");
?>

<script class="jsbin" src="<?php echo BASE_URL;?>assets/js/jquery.min.js"></script>
<script>var base_url = '<?php echo BASE_URL;?>'</script>

</head>
<body>
<!--wrapper start-->
<div class="wrapper">
  <!--header menu start-->
  <div class="header">
                <div class="header-menu">
                    <div class="title">Mega <span>Bolão</span></div>
                    <div class="sidebar-btn menu-btn">
                        <i class="fas fa-bars"></i>
                    </div>
                    <ul>
                        <li>
                        	<div class="searchbar">
                        		<input type="text" placeholder="Pesquisar">
                        		<a href="#" class="icon"><i class="fas fa-search"></i></a>
                        	</div><!-- searchbar-->
                        </li>

                        <li class="drop_noti"><a href="javascript:;"><i class="fas fa-bell"></i></a>
                        	<div class="dd_menu">
                               <ul>
                               <li><a href="javascript:void(0);">Location</a></li>	
                               <li><a href="javascript:void(0);">Favorites</a></li>
                               <li><a href="javascript:void(0);">Alterar senha</a></li>
                               </ul>
                            </div>
                        </li>

                        <li><a href="<?php echo BASE_URL;?>usuarios/logout"><i class="fas fa-power-off"></i></a></li>
                    </ul>
                </div>
  </div>
<!--header menu end-->



<div class="menu-btn">
<i class="fas fa-bars"></i>	
</div>

<div class="side-bar">

<center class="profile">
 <img src="<?php echo BASE_URL;?>assets/images/usuario.png" alt="Imagem usuário">
 <p>Nome do usuário</p>
</center>
<!-- <div class="close-btn">
	 <i class="fas fa-times"></i>
</div> -->

<div class="menu">
<div class="item"><a href="#" class="active"><i class="fas fa-desktop"></i>Dashboard</a></div>
<div class="item">
<a class="sub-btn" href="javascript:void(0);"><i class="fas fa-table"></i>Tabelas <i class="fas fa-angle-right dropdown"></i></a>

<div class="sub-menu">
<a href="#" class="sub-item">Sub item 1</a>	
<a href="#" class="sub-item">Sub item 2</a>	
<a href="#" class="sub-item">Sub item 3</a>	
</div>

</div>
<div class="item"><a href="#"><i class="fas fa-th"></i>Forms</a></div>
<div class="item">
<a href="javascript:void(0);" class="sub-btn"><i class="fas fa-cogs"></i>Configurações <i class="fas fa-angle-right dropdown"></i></a>
<div class="sub-menu">
<a href="#" class="sub-item">Sub item 1</a>	
<a href="#" class="sub-item">Sub item 2</a>	
<a href="#" class="sub-item">Sub item 3</a>	
</div>
</div>
<div class="item"><a href="#"><i class="fas fa-info-circle"></i>Sobre</a></div>
</div>

</div>

<main class="main">
<?php 
   $this->loadViewInTemplate($viewName, $viewData); 
?>  

</main>

<script src="<?php echo BASE_URL;?>assets/js/system.js"></script>

<?php
if(isset($viewData['list_js']) && !empty($viewData['list_js'])):
  foreach($viewData['list_js'] as $value):
     echo '<script src="'.BASE_URL.'assets/js/'.$value.'.js"></script>'.PHP_EOL;
  endforeach;
endif;
?>
</body>
</html>