<div class="content_page">

<div class="box-header">
<h2>Adicionar cliente</h2>

<div class="btns">
<button type="button" class="btn"><a href="<?php echo BASE_URL;?>clientes">Voltar</a></button>
</div>
</div>



<div class="conteudo">
<ul class="mytabs">
  <li data-mostrar="item1" class="ativa">Perfil</li>
  <li data-mostrar="item2">Endereço</li>   
</ul>

<form method="post" id="form_cliente" class="form" action="<?php echo BASE_URL;?>clientes/add_action">

<div id="item1" class="tabConteudo">  

<div class="group-form w50">
<label>Nome completo:</label>
<input type="text" name="nome" required/>
</div><!-- input wrapper-->

<div class="group-form w50">
  <label>Data Aniversário:</label>
  <input type="text" name="data_aniversario" class="date" />
</div><!-- input wrapper--> 


<div class="group-form w50">
  <label>E-mail:</label>
  <input type="email" name="email" />
</div><!-- input wrapper-->


<div class="group-form w50">
  <label>Senha:</label>
  <input type="password" name="senha" data-parsley-minlength="3" />
</div><!-- input wrapper-->



<div class="group-form w30">  
<label>Contato 1 <h6 style="display: inline-block;color: tomato;font-size: 14px;">*</h6></label>

<div class="group-icons">
 <i class="fa fa-phone" data-input="telefone1" onclick="changeMask(this)"></i>  
 <i class="fa fa-mobile" data-input="telefone1" onclick="changeMask(this)"></i>
 <input type="text" name="telefone1" id="telefone1" class="celular" />
</div>
</div><!-- form-group-->

<div class="group-form w30">  
<label>Contato 2 <h6 style="display: inline-block;color: tomato;font-size: 14px;">*</h6></label>

<div class="group-icons">
 <i class="fa fa-phone" data-input="telefone2" onclick="changeMask(this)"></i>  
 <i class="fa fa-mobile" data-input="telefone2" onclick="changeMask(this)"></i>
 <input type="text" name="telefone2" id="telefone2" class="celular"/>
</div>
</div><!-- form-group-->

<div class="group-form w30">
<label>Patrocinador:</label>
<select name="patrocinador">
  <option value="">Selecione o patrocinador</option>
  <?php foreach($clientes as $cliente):?>
  <option value="<?php $cliente->slug;?>"><?php echo $cliente->nome;?></option>
<?php endforeach;?>
</select>
</div>

<div class="group-form w100">
<label>Observação</label>
<textarea name="observacao"></textarea>
</div>

</div><!-- tab 1 -->

<div id="item2" class="tabConteudo">  

<div class="group-form w50">
<label>Cep:</label>
<input type="text" name="cep" class="cep" />
</div><!-- input wrapper--> 

<div class="group-form w50">
<label>Endereço:</label>
<input type="text" name="endereco" />
</div><!-- input wrapper--> 

    <div class="group-form w50">
      <label>Complemento:</label>
      <input type="text" name="complemento" />
    </div><!-- input wrapper--> 

    <div class="group-form w50">
      <label>Numero:</label>
      <input type="text" name="numero" />
    </div><!-- input wrapper--> 

    <div class="group-form w50">
      <label>Bairro:</label>
      <input type="text" name="bairro" />
    </div><!-- input wrapper--> 

        <div class="group-form w50">
      <label>Referencia:</label>
      <input type="text" name="referencia" />
    </div><!-- input wrapper--> 

</div><!-- Tab 2 -->

<div class="group-form w100">
  <input type="submit" name="submit" class="btn" value="Salvar" />      
</div><!-- input wrapper-->  
   
</form>  
</div><!-- content page -->

<script>
 $(document).ready(function(){   
   $('#form_cliente').parsley();
 }); 


function changeMask(obj){   
let input = $(obj).data('input');
let classe = $(obj).attr('class');

if(classe == 'fa fa-mobile'){
 $('#'+input).val('').removeClass('celular').addClass('telefone').attr('maxlength',14);
}else if(classe == 'fa fa-phone'){
 $('#'+input).val('').removeClass('telefone').addClass('celular').attr('maxlength',15);
}

}
</script>