<?php
$titulo = $info->titulo ?? '';
$endereco = $info->endereco ?? '';
$numero_contato = $info->numero_contato ?? '';
$numero_zap = $info->numero_zap ?? '';
$valor_comissao = $info->valor_comissao ?? '';
$pedido_minimo = $info->pedido_minimo ?? '';
$validade_bonus = $info->validade_bonus ?? '';
$logomarca = $info->logomarca ?? '';
$favicon = $info->favicon ?? '';
$termos = $info->termos ?? '';
$politica = $info->politica ?? '';
$sobre = $info->sobre ?? '';
$faq = $info->faq ?? '';
?>

<div class="content_page">

<div class="box-header">
<h2 style="color: #555;">Ajustar configurações do sistema</h2>


</div>



<form method="post" id="form_categoria" class="form" action="<?php echo BASE_URL;?>config/add_action/<?php echo $id_config;?>" enctype="multipart/form-data">


<div class="group-form w30">
<label>Titulo website:</label>
<input type="text" name="nome" required value="<?php echo $titulo;?>" />
</div><!-- input wrapper-->

<div class="group-form w30">
<label>Endereço:</label>
<input type="text" name="endereco" required value="<?php echo $endereco;?>"/>
</div><!-- input wrapper-->

<div class="group-form w30">
<label>Número contato:</label>
<input type="text" name="numero_contato" class="celular" value="<?php echo $numero_contato;?>" />
</div><!-- input wrapper-->

<div class="group-form w30">
<label>Número whatsapp:</label>
<input type="text" name="numero_zap" class="celular"  value="<?php echo $numero_zap;?>" />
</div><!-- input wrapper-->

<div class="group-form w30">
<label>Pedido minimo:</label>
<input type="text" name="pedido_minimo" value="<?php echo $pedido_minimo;?>" />
</div><!-- input wrapper-->

<div class="group-form w30">
<label>Comissão rede de indicacores:</label>
<input type="text" name="comissao_bonus" class="money" value="<?php echo $validade_bonus;?>" />
</div><!-- input wrapper-->


<div class="group-form w100">

<div class="group-form w20">
<label>Logomarca:</label>

<div class="container-preview">
  <a href="javascript:;" class="remove_image" style="display: block;">X</a>
  <img src="<?php echo BASE_URL.$logomarca;?>" id="image" style="display: block;">
</div><!--  container preview-->

<input type="file" name="logomarca" data-image="image" id="logomarca" hidden accept="image/jpg,image/jpeg,image/png" onchange="mostrarImagem(this)"/>
<label for="logomarca" class="input_file">Selecione um arquivo</label>  
</div><!-- input wrapper--> 

<div class="group-form w20">
<label>Favicon:</label>

<div class="container-preview">
  <a href="javascript:;" class="remove_image" style="display: block;">X</a>
  <img src="<?php echo BASE_URL.$favicon;?>" id="image_icon" style="display: block;">
</div><!--  container preview-->

<input type="file" name="favicon" data-image="image_icon" id="favicon" hidden accept="image/jpg,image/jpeg,image/png" onchange="mostrarImagem(this)"/>
<label for="favicon" class="input_file">Selecione um arquivo</label>     
</div><!-- input wrapper-->

</div> 



<div class="group-form w100">

<div class="group-form w50">
<label>Termos e contrato</label>
<textarea name="termos" id="summernote"><?php echo $termos;?></textarea>
</div>

<div class="group-form w50">
<label>Politicas de privacidade</label>
<textarea name="politicas" id="summernote2"><?php echo $politica;?></textarea>
</div>

<div class="group-form w50">
<label>Sobre</label>
<textarea name="sobre" id="summernote3"><?php echo $sobre;?></textarea>
</div>

<div class="group-form w50">
<label>Faq</label>
<textarea name="faq" id="summernote4"><?php echo $faq;?></textarea>
</div>

</div>



<div class="group-form w100">
  <input type="submit" name="submit" class="btn" value="Salvar" />      
</div><!-- input wrapper-->  
   
</form>  
</div><!-- content page -->

<script>
 $(document).ready(function(){   
   $('#form_config').parsley();
 });
</script>

<?php 
if(isset($msg)):
 echo '<script>
          $(document).ready(function(){
           fncSweetAlert("success", "'.$msg.'", "");
          });
       </script>';
endif;
?>