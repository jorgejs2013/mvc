<?php
$nome = $info->nome_categoria ?? '';
$imagem = $info->img_categoria ?? '';
$icon = $info->icon_categoria ?? '';
?>

<div class="content_page">

<div class="box-header">
<h2>Configurações do site cliente</h2>

<div class="btns">
<button type="button" class="btn"><a href="<?php echo BASE_URL;?>categorias">Voltar</a></button>
</div>
</div>



<form method="post" id="form_categoria" class="form" action="<?php echo BASE_URL;?>categorias/edit_action/<?php echo $id_categoria;?>" enctype="multipart/form-data">


<div class="group-form w100">
<label>Nome:</label>
<input type="text" name="nome" required value="<?php echo $nome;?>" />
</div><!-- input wrapper-->


<div class="group-form w100">
<label>Imagem:</label>

<div class="container-preview">
  <a href="javascript:;" class="remove_image" style="display: block;">X</a>
  <img src="<?php echo BASE_URL.$imagem;?>" id="image" style="display: block;">
</div><!--  container preview-->

<input type="file" name="imagem_categoria" data-image="image" id="img_preview" hidden accept="image/jpg,image/jpeg,image/png" onchange="mostrarImagem(this)"/>
<label for="img_preview" class="input_file">Selecione um arquivo</label>  
</div><!-- input wrapper--> 

<div class="group-form w100">
<label>Icon categoria:</label>

<div class="container-preview">
  <a href="javascript:;" class="remove_image" style="display: block;">X</a>
  <img src="<?php echo BASE_URL.$icon;?>" id="image_icon" style="display: block;">
</div><!--  container preview-->

<input type="file" name="image_icon" data-image="image_icon" id="img_icon" hidden accept="image/jpg,image/jpeg,image/png" onchange="mostrarImagem(this)"/>
<label for="img_icon" class="input_file">Selecione um arquivo</label>     
</div><!-- input wrapper--> 



<div class="group-form w100">
  <input type="submit" name="submit" class="btn" value="Salvar" />      
</div><!-- input wrapper-->  
   
</form>  
</div><!-- content page -->

<script>
 $(document).ready(function(){   
   $('#form_categoria').parsley();
 });
</script>