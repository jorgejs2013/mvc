<div class="content_page">

<div class="box_form">
<div class="box_form_title">
  <h3>Mudar senha de acesso</h3>
  
</div><!-- box form title -->

<div class="box_form_content">
   <form method="post" id="form_reset" enctype="multipart/form-data" class="form">
    
    <div class="group-form w100">
      <label>Senha anterior:</label>
      <input type="password" name="old_senha" placeholder="*****" data-parsley-required="true" />
    </div><!-- input wrapper--> 

    <div class="group-form w100">
      <label>Nove senha:</label>
      <input type="password" name="senha" id="password" data-parsley-length="[6,12]"  placeholder="*****"/>
    </div><!-- input wrapper--> 

    <div class="group-form w100">
      <label>Confirmar senha:</label>
      <input type="password" name="csenha" data-parsley-length="[6,12]" data-parsley-equalto="#password" placeholder="*****" />
    </div><!-- input wrapper--> 
  


    <div class="group-form w100">
      <input type="submit" name="submit" class="btn" value="Atualizar" />
     
     </div><!-- input wrapper-->  
   

</div><!-- box form content -->
</div><!-- box form -->  
</div><!-- content page -->

<?php
if(isset($msg)):
 echo '<script>
          $(document).ready(function(){
           fncSweetAlert("success", "'.$msg.'", "");
          });
       </script>';
endif;
?>