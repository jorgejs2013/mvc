<?php
$status = (isset($info_usuario->status)) ? $info_usuario->status : '';
$tipo_usuario = (isset($info_usuario->tipo_usuario)) ? $info_usuario->tipo_usuario : '';
$nome = (isset($info_usuario->nome)) ? $info_usuario->nome : '';
$email = (isset($info_usuario->email)) ? $info_usuario->email : '';
$imagem = (isset($info_usuario->imagem)) ? $info_usuario->imagem : '';

$telefone1 = (isset($info_usuario->telefone1)) ? $info_usuario->telefone1 : '';
$telefone2 = (isset($info_usuario->telefone2)) ? $info_usuario->telefone2 : '';
$cargo = (isset($info_usuario->cargo)) ? $info_usuario->cargo : '';
$setor = (isset($info_usuario->setor)) ? $info_usuario->setor : '';
$admin = (isset($info_usuario->admin)) ? $info_usuario->admin : '';
$observacoes = (isset($info_usuario->observacoes)) ? $info_usuario->observacoes : '';
?>
<div class="content_page">

<div class="box-header">
<h2>Editar usuário</h2>
<div class="btns">
<button type="button" class="btn"><a href="<?php echo BASE_URL;?>usuarios">Voltar</a></button>
</div>
</div>

<div class="box_form_content">
   <form method="post" id="form_usuario" enctype="multipart/form-data" class="form" action="<?php echo BASE_URL."usuarios/edit_action/". $id_user;?>">
    
<div class="group-form w30">
<label>Data de cadastro:</label>
<input type="text" name="nome" value="<?php echo date('d/m/Y H:i:s');?>"  readonly disabled/>
</div><!-- input wrapper--> 

<div class="group-form w30">
<label>Status</label>
<select name="status">
 <option value="interno">Interno</option> 
 <option value="externo">Externo</option> 
</select>
</div>

<div class="group-form w30">
<label>Tipo de usuário</label>
<select name="tipo_usuario">
 <option value="ativo" <?php echo ($tipo_usuario == 'ativo')? 'selected': '';?>>Ativo</option> 
 <option value="inativo" <?php echo ($tipo_usuario == 'inativo')? 'selected': '';?>>Inativo</option> 
</select>
</div>

    <div class="group-form w30">
      <label>Nome:</label>
      <input type="text" name="nome" placeholder="Informe o nome completo" value="<?php echo $nome;?>" />
    </div><!-- input wrapper--> 

         
<div class="group-form w30">
  <label>Telefone:</label>
  <input type="text" name="telefone1" class="phone" placeholder="(99) 9999-9999" value="<?php echo $telefone1;?>"/>
</div><!-- input wrapper-->

<div class="group-form w30">
  <label>Telefone 2:</label>
  <input type="text" name="telefone2" class="phone" placeholder="(99) 9999-9999" value="<?php echo $telefone2;?>"/>
</div><!-- input wrapper-->

<div class="group-form w30">
  <label>E-mail:</label>
  <input type="email" name="email" required="required" placeholder="example@example.com" value="<?php echo $email;?>"/>
</div><!-- input wrapper-->        

<div class="group-form w30">
<label>Cargo:</label>
<input type="text" name="cargo" placeholder="Informe o cargo" value="<?php echo $cargo;?>"/>      
</div><!-- input wrapper-->  

<div class="group-form w30">
<label>Setor:</label>
<input type="text" name="setor" placeholder="Informe o setor" value="<?php echo $setor;?>"/>      
</div><!-- input wrapper-->  

<div class="group-form w30">
  <label>Senha:</label>
  <input type="password" name="password" minlength="4" maxlength="12" placeholder="******" />
</div><!-- input wrapper-->    

 

<div class="group-form w100">
<label>Observações:</label>
<textarea name="observacoes"><?php echo $observacoes;?></textarea>
</div><!-- input wrapper--> 
    

<div class="group-form w100">
  <input type="submit" name="submit" class="btn" value="Atualizar" />
</div><!-- input wrapper-->  
   

</div><!-- box form content -->
</div><!-- content page -->