<div class="content_page">

<div class="box-header">
<h2>Adicionar usuário</h2>

<div class="btns">
<button type="button" class="btn"><a href="<?php echo BASE_URL;?>usuarios">Voltar</a></button>
</div>
</div>

<div class="box_form_content">
   <form method="post" id="form_usuario" enctype="multipart/form-data" class="form" action="<?php echo BASE_URL."usuarios/add_action";?>">
    
<div class="group-form w30">
<label>Data de cadastro:</label>
<input type="text" name="nome" value="<?php echo date('d/m/Y H:i:s');?>"  readonly disabled/>
</div><!-- input wrapper--> 

<div class="group-form w30">
<label>Status</label>
<select name="status">
 <option value="interno">Interno</option> 
 <option value="externo">Externo</option> 
</select>
</div>

<div class="group-form w30">
<label>Tipo de usuário</label>
<select name="tipo_usuario">
 <option value="ativo">Ativo</option> 
 <option value="inativo">Inativo</option> 
</select>
</div>

    <div class="group-form w30">
      <label>Nome:</label>
      <input type="text" name="nome" placeholder="Informe o nome completo" />
    </div><!-- input wrapper--> 

<div class="group-form w30">
  <label>Telefone:</label>
  <input type="text" name="telefone1" class="phone" placeholder="(99) 9999-9999" />
</div><!-- input wrapper-->

<div class="group-form w30">
  <label>Telefone 2:</label>
  <input type="text" name="telefone2" class="phone" placeholder="(99) 9999-9999" />
</div><!-- input wrapper-->

<div class="group-form w30">
  <label>E-mail:</label>
  <input type="email" name="email" required="required" placeholder="example@example.com" />
</div><!-- input wrapper-->        

<div class="group-form w30">
<label>Cargo:</label>
<input type="text" name="cargo" placeholder="Informe o cargo" />      
</div><!-- input wrapper-->  

<div class="group-form w30">
<label>Setor:</label>
<input type="text" name="setor" placeholder="Informe o setor" />      
</div><!-- input wrapper-->  

<div class="group-form w30">
  <label>Senha:</label>
  <input type="password" name="password" minlength="4" maxlength="12" placeholder="******" />
</div><!-- input wrapper-->    

 

<div class="group-form w100">
<label>Observações:</label>
<textarea name="observacoes"></textarea>
</div><!-- input wrapper--> 
    

<div class="group-form w100">
  <input type="submit" name="submit" class="btn" value="Salvar" />
</div><!-- input wrapper-->  
   

</div><!-- box form content -->
</div><!-- content page -->