<div class="content_page">

<div class="titulo_botao">
<h2>Lista de usuários</h2>

<div class="btns">
<button type="button" class="btn"><a href="<?php echo BASE_URL;?>usuarios/add">Adicionar</a></button>
</div>

</div><!-- titulo botao -->

<form method="get" class="form" style="background: #222;margin-top: 0px;margin-bottom: 20px;">
  
<div class="group-form w50">
<label style="color: #fff;">Nome:</label>
<input type="text" name="nome" placeholder="Digite o nome">
</div>
<div class="group-form w25">
<button type="submit" class="btn btnLine"><i class="fa fa-search"></i></button>
</div>

</form>


<div class="container_shrinker">

<table class="table shrink">
<thead>
<tr>
	<th class="shrink-xs">Nome</th>
	<th class="shrink-xs shrinkable">E-mail</th>  
	<th class="shrink-xs">Ações</th>
</tr>	
</thead>	

<tbody>
<?php foreach($listaUsers as $usuario):?>	
<tr>
<td><?php echo $usuario->nome;?></td>	
<td><?php echo $usuario->email;?></td>
<td>
<a href="<?php echo BASE_URL;?>usuarios/edit/<?php echo $usuario->id;?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>  | 
<a href="<?php echo BASE_URL;?>usuarios/del/<?php echo $usuario->id;?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
|
<a href="<?php echo BASE_URL;?>permissao/index/<?php echo $usuario->id;?>">Permissão</a>		
</td>
</tr>
<?php endforeach;?>
	
</tbody>
</table>
</div><!-- table listagem-->

</section><!-- content page -->


<script>
$(document).ready(function(){
$('#limpar').click(function(){
window.location.href = base_url+"usuarios";   
});
});	
</script>

<?php if(isset($msg) && $msg != ''){ ?>
<script>
var msg = '<?php echo $msg;?>'; 
$(document).ready(function(){
Swal.fire({
  title: msg,  
  type: 'success',
  confirmButtonText: 'Fechar',
  confirmButtonColor: "#3085d6",
}).then((result) => { 
  if (result) {   
    
     
  }
}); 
});
</script>
<?php }?>
