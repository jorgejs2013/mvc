<section class="content_page">

<div class="titulo_botao">
<h2>Categorias</h2>

<div class="btns">
<button type="button" class="btn"><a href="<?php echo BASE_URL;?>categorias/add">Adicionar</a></button>
</div>

</div><!-- titulo botao -->

<form method="get" class="form" style="background: #222;margin-top: 0px;margin-bottom: 20px;">
<div class="group-form w50">
<label style="color: #fff;">Nome:</label>	
<input type="text" name="nome" placeholder="Digite o nome">
</div>

<div class="group-form w25">
  <button type="submit" class="btn btnLine"><i class="fa fa-search"></i></button>
</div>
</form>

<div class="container_shrinker">
<table class="table shrink">
<thead>
<tr>
	<th>Código</th>	
	<th>Nome</th>
	<th>Ação</th>	
</tr>	
</thead>	

<tbody>
	
<?php 
foreach ($lista as $categoria): ?>
<tr>
<td><?php echo str_pad($categoria->id_categoria, 5, "0", STR_PAD_LEFT) ;?></td>	
<td><?php echo $categoria->nome_categoria;?></td>	

<td>
<a href="<?php echo BASE_URL;?>categorias/edit/<?php echo $categoria->id_categoria;?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>  | 
<a href="<?php echo BASE_URL;?>categorias/del/<?php echo $categoria->id_categoria;?>" class="fncSweetDel"><i class="fa fa-trash" aria-hidden="true"></i></a>	
</td>
</tr>

<?php 
endforeach;
?>
		
</tbody>
</table>
</div><!-- container shrinker -->

<?php if(isset($paginacao)):echo $paginacao;endif;

if(isset($msg)):
 echo '<script>
          $(document).ready(function(){
           fncSweetAlert("success", "'.$msg.'", "");
          });
       </script>';
endif;
?>

</section><!-- content page -->