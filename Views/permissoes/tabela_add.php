<div class="content_page">

<div class="box-header">
<h2>Adicionar usuário</h2>

<div class="btns">
<button type="button" class="btn"><a href="<?php echo BASE_URL;?>usuarioTabela">Voltar</a></button>
</div>
</div>


<div class="box_form_content">
   <form method="post" id="form_tabela" class="form_box">

    <div class="group-form w100">
      <label>Nome:</label>
      <input type="text" name="nome" placeholder="Informe o Nome" />
    </div><!-- input wrapper-->

    <div class="group-form w100">
      <label>Ativa:</label>
      <select name="acao">
        <option value="S" selected="selected">Sim</option>
        <option value="N">Não</option>
      </select>
    </div><!-- input wrapper-->

   
  <div class="group-form w100">
   <input type="submit" class="btn" name="submit" value="Salvar" />
  </div>
   

</div><!-- box form content -->
</div><!-- box form -->  
</section><!-- form cad -->

</section>

<?php if(isset($msg) && $msg != ''){ ?>
<script>
var msg = '<?php echo $msg;?>'; 
$(document).ready(function(){
swal({
  title: msg,  
  type: 'success',
  confirmButtonText: 'Fechar',
  confirmButtonColor: "#3085d6",
}).then((result) => { 
  if (result) {
    $('#form_tabela')[0].reset();
    window.location.href = base_url+"usuarioTabela";   
     
  }
});
}); 
</script>
<?php }?>

<script>
 $(document).ready(function(){
  $('.money').mask('000.000.000.000.000,00', {reverse: true});
  $('.cep').mask('00000-000', {placeholder: "99999-999"});
 }) 
</script>