<div class="content_page">
<h2>Ações</h2>

<div class="box-header">

<div class="form_pesquisa">
<form method="get">
<input type="text" name="nome" placeholder="Digite o nome">
<button type="button"><i class="fa fa-search"></i></button>
</form>
</div><!-- form pesquisa-->

<div class="btns">
<button type="button" class="btn"><a href="<?php echo BASE_URL;?>usuarioAcao/add">Adicionar</a></button>
</div>
</div><!-- box header-->


<div class="container_shrinker">

<table class="table shrink">
	
<thead>
<tr>
<th>Id</th>	
<th>Nome</th>
<th class="shrink-xs" align="right">Ação</th>
</tr>
	
</thead>

<tbody>
<?php 
if(isset($list) && $list != "" ):
foreach($list as $acao): 
?>

<tr>
<td><?php echo $acao->id_acao;?></td>
<td><?php echo $acao->acao;?></td>

<td align="right">

<button type="button" class="btn btn-info"><a href="<?php echo BASE_URL;?>usuarioAcao/edit/<?php echo $acao->id_acao;?>">Editar</a></button>
<button type="button" class="btn btn-error">
<a href="<?php echo BASE_URL;?>usuarioAcao/del/<?php echo $acao->id_acao;?>">Apagar</a>
</button>
</td>
</tr>

<?php 
endforeach;
endif;
?>
	
</tbody>	

</table>
</div><!-- container_shrinker -->

</section>

<?php if(isset($msg) && $msg != ''){ ?>
<script>
var msg = '<?php echo $msg;?>'; 
$(document).ready(function(){
Swal.fire({
  title: msg,  
  type: 'success',
  confirmButtonText: 'Fechar',
  confirmButtonColor: "#3085d6",
}).then((result) => { 
  if (result) {    
     
  }
}); 
});
</script>
<?php }?>