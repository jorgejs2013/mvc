<?php
$nome = (isset($info->acao)) ? $info->acao : null;
?>


<div class="content_page">

<div class="box-header">
<h2>Adicionar usuário</h2>

<div class="btns">
<button type="button" class="btn"><a href="<?php echo BASE_URL;?>usuarioacao">Voltar</a></button>
</div>
</div>


<div class="box_form_content">
   <form method="post" id="form_acao" class="form_box">

    <div class="group-form w100">
      <label>Nome:</label>
      <input type="text" name="nome" placeholder="Ex: inserir, editar, visualizar..."  value="<?php echo $nome;?>" />
    </div><!-- input wrapper--> 
   
<div class="group-form w100">
<input type="submit" name="submit" value="Atualizar" class="btn btn-success" />
</div><!-- input wrapper-->   

</form>
</div><!-- box form content -->
</div><!-- box form -->  

</section>

<?php if(isset($msg) && $msg != ''){ ?>
<script>
var msg = '<?php echo $msg;?>'; 
swal({
  title: msg,  
  type: 'success',
  confirmButtonText: 'Fechar',
  confirmButtonColor: "#3085d6",
}).then((result) => { 
  if (result) {
    $('#form_acao')[0].reset();
    window.location.href = base_url+"usuarioAcao";   
     
  }
}); 
</script>
<?php }?>


<script type="text/javascript">
$(document).ready(function(){
var form = $('#form_acao');  
$('#submitForm').click(function(e){
e.preventDefault();
form.submit();
});
}); 
</script>
