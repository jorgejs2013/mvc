<?php
$nome = (isset($info->nome_tabela)) ? $info->nome_tabela : null;
$acao = (isset($info->ativo_tabela)) ? $info->ativo_tabela : null;
?>

<div class="content_page">

<div class="box-header">
<h2>Editar tabela</h2>

<div class="btns">
<button type="button" class="btn"><a href="<?php echo BASE_URL;?>usuarioTabela">Voltar</a></button>
</div>
</div>


<div class="box_form_content">
   <form method="post" id="form_tabela" class="form_box">

    <div class="group-form w100">
      <label>Nome:</label>
      <input type="text" name="nome" placeholder="Informe o Nome" value="<?php echo $nome;?>" />
    </div><!-- input wrapper-->

    <div class="group-form w100">
      <label>Ação:</label>
      <select name="acao">
        <option value="S" <?php echo ($acao == 'S') ? 'selected="selected"' : null;?> >Sim</option>
        <option value="N" <?php echo ($acao == 'N') ? 'selected="selected"' : null;?>>Não</option>
      </select>
    </div><!-- input wrapper-->

   <div class="group-form w100">
    <input type="submit" class="btn btn-success" name="submit" value="Atualizar" />  
  </div><!-- input wrapper-->
  
   </form>

</div><!-- box form content -->
</div><!-- box form -->  

</section>