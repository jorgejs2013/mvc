<section class="content_page">

<div class="box_form">

<div class="box_form_title">
  <h3>Adicionar</h3>
  <span class="min_box">
    <a href="<?php echo BASE_URL;?>contato"><i class="fa fa-arrow-left" style="color: white;" aria-hidden="true"></i></a></span>
</div><!-- box form title -->


<div class="box_form_content">

<ul class="js-tabmenu">
  <li>Geral</li>
  <li>Endereço</li>
  <li>Extras</li> 
</ul>

<form method="post" id="form_contato" class="form_box" action="<?php echo BASE_URL;?>contato/add_action">

<div class="js-tabcontent">
<section>    
<div class="input-wrapper w100">

<div class="input-wrapper w50"> 
<div class="input-wrapper w25">
    <span>Cliente:</span>
     <input id="checkbox1" class="custom_checkbox" checked="checked" name="eh_cliente" type="checkbox">
      <label for="checkbox1" data-text-true="Sim" data-text-false="Não"><i></i></label>
</div><!-- input wrapper--> 

<div class="input-wrapper w25">
    <span>Funcionario:</span>
     <input id="checkbox3" class="custom_checkbox" name="eh_funcionario" type="checkbox">
      <label for="checkbox3" data-text-true="Sim" data-text-false="Não"><i></i></label>
</div><!-- input wrapper-->

<div class="input-wrapper w25">
    <span>Motorista:</span>
     <input id="checkbox4" class="custom_checkbox" name="eh_motorista" type="checkbox">
      <label for="checkbox4" data-text-true="Sim" data-text-false="Não"><i></i></label>
</div><!-- input wrapper-->

</div><!-- input wrapper-->
</div><!-- input wrapper-->



    <div class="input-wrapper w50">
      <span>Nome:</span>
      <input type="text" name="nome" data-parsley-minlength="3" data-parsley-required="true"/>
    </div><!-- input wrapper--> 

     <div class="input-wrapper w50">
      <span>Fantasia:</span>
      <input type="text" name="fantasia" data-parsley-minlength="3" />
    </div><!-- input wrapper-->  

     <div class="input-wrapper w30">
      <span>CPF:</span>
      <input type="text" name="cpf" class="cpf" data-parsley-minlength="3" />
    </div><!-- input wrapper--> 

     <div class="input-wrapper w30">
      <span>CNPJ:</span>
      <input type="text" name="cnpj" class="cnpj" data-parsley-minlength="3" />
    </div><!-- input wrapper--> 

    <div class="input-wrapper w30">
      <span>Data Nascimento:</span>
      <input type="text" name="data_nascimento" data-parsley-minlength="3" data-parsley-required="true" placeholder="dd/mm/yyyy" date-id="datetime" class="calendario" data-date-format="d/m/Y" />
    </div><!-- input wrapper--> 

    <div class="input-wrapper w30">
      <span>DDD:</span>
      <input type="text" name="ddd" data-parsley-minlength="2" data-parsley-maxlength="4" />
    </div><!-- input wrapper--> 

    <div class="input-wrapper w30">
      <span>Telefone:</span>
      <input type="text" name="telefone" class="telefone" data-parsley-minlength="3" />
    </div><!-- input wrapper--> 


    <div class="input-wrapper w30">
      <span>Celular:</span>
      <input type="text" name="celular" class="celular" data-parsley-minlength="3" />
    </div><!-- input wrapper--> 

    <div class="input-wrapper w50">
      <span>E-mail:</span>
      <input type="email" name="email" data-parsley-minlength="3" />
    </div><!-- input wrapper-->

    <div class="input-wrapper w50">
      <span>Senha:</span>
      <input type="password" name="senha" data-parsley-minlength="3" />
    </div><!-- input wrapper-->     
</section><!--geral -->


<section>

<div class="input-wrapper w50">  
    <span>Cep:</span>
      <input type="text" name="cep" class="cep" data-parsley-minlength="3" />
</div><!-- input wrapper-->

<div class="input-wrapper w50">  
    <span>Logradouro:</span>
      <input type="text" name="logradouro" data-parsley-minlength="3" />
</div><!-- input wrapper-->

<div class="input-wrapper w30">  
    <span>Número:</span>
      <input type="text" name="numero" />
</div><!-- input wrapper-->


<div class="input-wrapper w30">  
    <span>UF:</span>
      <select name="uf" required="required">
        <option value="0" disabled="disabled">Selecione o estado</option>
        <?php 
         foreach($estados as $estado):  
         ?>

         <option value="<?php echo $estado->id_estado;?>" <?php echo ($estado->id_estado == '5') ? 'selected="selected"' : '';?>><?php echo $estado->nome_estado;?></option>
         
         <?php 
         endforeach;
        ?>
      </select>
</div><!-- input wrapper-->

<div class="input-wrapper w30">  
    <span>Cidade:</span>
      <select name="cidade" required="required">
        <option value="0" selected="selected" disabled="disabled">Selecione a cidade</option>
        <?php 
         foreach($cidades as $cidade):
          echo '<option value="'.$cidade->id_cidade.'">'.$cidade->nome_cidade.'</option>';
         endforeach;
        ?>
      </select>
</div><!-- input wrapper-->

<div class="input-wrapper w50">  
    <span>Complemento:</span>
      <input type="text" name="complemento" data-parsley-minlength="3" />
</div><!-- input wrapper-->

<div class="input-wrapper w50">  
    <span>Bairro:</span>
      <input type="text" name="bairro" data-parsley-minlength="3" />
</div><!-- input wrapper-->

</section>  <!-- Endereço -->



<section>
<div class="input-wrapper w50">  
    <span>Insc. Estadual:</span>
      <input type="text" name="ie" data-parsley-minlength="3" />
</div><!-- input wrapper-->

<div class="input-wrapper w50">  
    <span>Insc. Municipal:</span>
      <input type="text" name="im" data-parsley-minlength="3" />
</div><!-- input wrapper-->

<div class="input-wrapper w50">  
    <span>RG:</span>
      <input type="text" name="rg" data-parsley-minlength="3" />
</div><!-- input wrapper-->

<div class="input-wrapper w50">  
    <span>Suframa:</span>
      <input type="text" name="suframa" data-parsley-minlength="3" />
</div><!-- input wrapper-->

<div class="input-wrapper w50">  
    <span>Cód. Estrangeiro:</span>
      <input type="text" name="cod_estrangeiro" data-parsley-minlength="3" />
</div><!-- input wrapper-->

<div class="input-wrapper w50">  
    <span>Subs. Tributária:</span>
      <input type="text" name="ie_subt_trib" data-parsley-minlength="3" />
</div><!-- input wrapper-->
</section>

</div><!-- jb-content-->   
    

    <div class="input-wrapper w100">
      <input type="submit" name="submit" class="btn" value="Salvar" />      
    </div><!-- input wrapper-->  
   
</form>
</div><!-- box form content -->
</div><!-- box form -->  
</section><!-- content page -->


<script>
 $(document).ready(function(){   
   $('#form_contato').parsley();
 }); 
</script>