<?php
$nome_grupo = (isset($info->nome_grupo)) ? $info->nome_grupo: '';
?>
<div class="content_page">

<div class="box-header">
<h2>Adicionar grupo</h2>

<div class="btns">
<button type="button" class="btn"><a href="<?php echo BASE_URL;?>grupos">Voltar</a></button>
</div>
</div>

<div class="box_form_content">
   <form method="post" id="form_grupo" class="form" action="<?php echo BASE_URL;?>grupos/edit_action/<?php echo $id_grupo;?>">

    <div class="group-form w100">
      <label>Nome:</label>
      <input type="text" name="nome" placeholder="Informe o Nome" value="<?php echo $nome_grupo;?>" />
    </div><!-- input wrapper-->

    </div><!-- input wrapper--> 
  
   <div class="group-form w100">
      <input type="submit" name="submit" class="btn" value="Salvar" />     
     </div><!-- input wrapper-->  

</div><!-- box form content -->
</div><!-- box form -->  
</div><!-- content page -->


<script>
 $(document).ready(function(){   
   $('#form_grupo').parsley();
 }); 
</script>