<section class="content_page">	

<div class="titulo_botao">
<h2>Grupos</h2>

<div class="btns">
<button type="button" class="btn"><a href="<?php echo BASE_URL;?>grupos/add">Adicionar</a></button>
</div>

</div><!-- titulo botao -->


<form method="post" class="form" style="background: #222;margin-top: 0px;margin-bottom: 20px;">
  
<div class="group-form w50">
<label style="color: #fff;">Nome:</label>
<input type="text" name="nome" placeholder="Digite o nome">
</div>
<div class="group-form w25">
<button type="submit" class="btn btnLine"><i class="fa fa-search"></i></button>
</div>

</form>

<div class="container_shrinker">
<table class="table shrink">
<thead>
<tr>
	<th class="shrink-xs">Código</th>	
	<th>Nome</th>		
	<th class="shrink-xs">Ação</th>	
</tr>	
</thead>	

<tbody>
<?php foreach ($lista as $grupo): ?>
<tr>
<td><?php echo str_pad($grupo->id_grupo, 5, "0", STR_PAD_LEFT) ;?></td>	
<td><?php echo $grupo->nome_grupo;?></td>	


<td>
<a href="<?php echo BASE_URL;?>grupos/edit/<?php echo $grupo->id_grupo;?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>  | 
<a href="<?php echo BASE_URL;?>grupos/del/<?php echo $grupo->id_grupo;?>"><i class="fa fa-trash" aria-hidden="true"></i></a>	
</td>
</tr>

<?php endforeach;?>
		
</tbody>
</table>

<?php
if(isset($pagination)):
echo $pagination;
endif;
?>
</div><!-- table listagem-->

</section><!-- content page -->

<script>
$(document).ready(function(){
$('#limpar').click(function(){
window.location.href = base_url+"grupos";   
});
});	
</script>
