<?php
use Core\Controller;
use Models\Users;
use Models\Permissao;
use Models\Orm;
use Models\Search;

class ticketsController extends controller {

private $user;
private $arr;

 public function __construct() {              
        $this->user = new Users();

        if($this->user->isLogged() == false){                  
         header("Location: ".BASE_URL."login");        
         exit;         
        }


        $this->arr = array(
         'user'=>$this->user,
         'menuActive'=>'configuracoes',
         'bread'=>'Tickets'        
        );          
}

public function index() {
$this->arr['list_js'] = array(
'plugins/jquery.table-shrinker',
'shrinker_init', 
'plugins/jquery.mask.min',
'mask_init',
'plugins/sweetalert2.all.min'
);     

$id_company  = $this->user->getCompany();        
$cliente = new Orm('clientes');


//Filtrando pesquisas
$dados['filter'] = array('nome'=>'');
if(isset($_GET['nome']) && !empty($_GET['nome'])){
$dados['filter']['nome'] = $_GET['nome'];

$s = new Search();
$this->arr['lista'] = $s->getAllSearch('clientes', $dados['filter']);
}else{
$this->arr['lista'] = $cliente->select('*')->where(['id_company', $id_company])->paginate('5')->get();
$this->arr['paginacao'] = $cliente->render('clientes'); 
}
 

$this->arr['msg'] = $this->flashMessage($_SESSION['msg'] ?? null);

$this->loadTemplate('tickets/home', $this->arr);
}//index


public function add(){
$this->arr['list_js'] = array(
'plugins/jquery.mask.min',
'mask_init',
'plugins/parsley/parsley.min',
'plugins/parsley/pt-br',
'tabs'
);   

$id_company  = $this->user->getCompany();       

        if(isset($_GET['nome'])){
            $dados['nome_form'] = $_GET['nome'];
        }
        if(isset($_GET['contato'])){
            $dados['contato_form'] = $_GET['contato'];
        }


$bairros = new Orm('bairros');
$dados['lista_bairros'] = $bairros->select('*')->where(['id_company', $id_company])->get();


  

$this->loadTemplate('clientes/adicionar', $this->arr);
}//add


public function add_action(){
$id_company  = $this->user->getCompany();  

if(isset($_POST['nome']) && !empty($_POST['nome'])):
$nome = $this->limpaCampo($_POST['nome']);
$aniversario = $this->limpaCampo($_POST['data_aniversario']); 
$email = $this->limpaCampo($_POST['email']);
$senha = $this->limpaCampo($_POST['senha']);
if($senha == null){
   $novaSenha =  $this->geraCodigo('6');   
}else{
   $novaSenha = password_hash($senha, PASSWORD_DEFAULT);
}           
               
$telefone1 = $this->limpaCampo($_POST['telefone1']);
$telefone2 = $this->limpaCampo($_POST['telefone2']);      
$observacao = $this->limpaCampo($_POST['observacao']); 

$cep         = $this->limpaCampo($_POST['cep']); 
$logradouro  = $this->limpaCampo($_POST['logradouro']); 
$complemento = $this->limpaCampo($_POST['complemento']); 
$numero      = $this->limpaCampo($_POST['numero']); 
$bairro      = $this->limpaCampo($_POST['bairro']); 
$referencia  = $this->limpaCampo($_POST['referencia']);  


$cliente = new Orm('clientes');
$insere = $cliente->set([
'id_company'=>$id_company,         
'nome'=>$nome, 
'data_nasc'=>$this->converterData($aniversario, '/'),        
'telefone1'=>$telefone1, 
'telefone2'=>$telefone2,         
'email'=>$email,
'senha'=>$senha,
'observacao'=>$observacao, 
'data_cad'=>date('Y-m-d')
])->save();         


if($logradouro != ''){
 $end = new Orm('endereco');
 $grava_endereco = $end->set([
  'id_cliente'=>$insere,
  'cep'=>$cep,
  'logradouro'=>$logradouro,
  'complemento'=>$complemento,
  'numero'=>$numero,
  'bairro'=>$bairro,
  'referencia'=>$referencia      
 ])->save();  
 }

 if($insere){
    $_SESSION['msg'] = 'Cliente cadastrado com sucesso!';
    $this->redirect(BASE_URL."clientes");
}  else{
    $_SESSION['msg'] = 'Ooops! erro ao cadastrar novamente!';
    $this->redirect(BASE_URL."clientes");
}

    
endif;
}//add action

public function edit($id){
$this->arr['list_js'] = array(
'plugins/jquery.mask.min',
'mask_init',
'plugins/parsley/parsley.min',
'plugins/parsley/pt-br',
'tabs'
); 

$id_company  = $this->user->getCompany();

$cliente = new Orm('clientes');
$this->arr['info_cliente'] = $cliente->select('*')->where(['id', $id])->e(['id_company', $id_company])->first()->get();
$this->arr['info_cliente']->endereco = (new Orm('endereco'))->select('*')->where(['id_cliente', $id])->get();
$this->arr['id_cliente'] = $id;

$bairros = new Orm('bairros');
$this->arr['lista_bairros'] = $bairros->select('*')->where(['id_company', $id_company])->get();

  $this->loadTemplate('clientes/editar', $this->arr);
}//edit



public function edit_action($id){
$id_company  = $this->user->getCompany();  

if(isset($_POST['nome']) && !empty($_POST['nome'])):
$nome = $this->limpaCampo($_POST['nome']);
$aniversario = $this->limpaCampo($_POST['data_aniversario']); 
$email = $this->limpaCampo($_POST['email']);
$senha = $this->limpaCampo($_POST['senha']);
if($senha == null){
   $novaSenha =  $this->geraCodigo('6');   
}else{
   $novaSenha = password_hash($senha, PASSWORD_DEFAULT);
}           
               
$telefone1 = $this->limpaCampo($_POST['telefone1']);
$telefone2 = $this->limpaCampo($_POST['telefone2']);      
$observacao = $this->limpaCampo($_POST['observacao']); 

$id_endereco = $this->limpaCampo($_POST['id_endereco']);
$cep         = $this->limpaCampo($_POST['cep']); 
$logradouro  = $this->limpaCampo($_POST['logradouro']); 
$complemento = $this->limpaCampo($_POST['complemento']); 
$numero      = $this->limpaCampo($_POST['numero']); 
$bairro      = $this->limpaCampo($_POST['bairro']); 
$referencia  = $this->limpaCampo($_POST['referencia']);  


$cliente = new Orm('clientes');
$insere = $cliente->set([
'id_company'=>$id_company,         
'nome'=>$nome, 
'data_nasc'=>$this->converterData($aniversario, '/'),        
'telefone1'=>$telefone1, 
'telefone2'=>$telefone2,         
'email'=>$email,
'senha'=>$senha,
'observacao'=>$observacao, 
'data_cad'=>date('Y-m-d')
])->where(['id', $id])->update();         


if($logradouro != ''){
 $end = new Orm('endereco');
 $grava_endereco = $end->set([
  'id_cliente'=>$insere,
  'cep'=>$cep,
  'logradouro'=>$logradouro,
  'complemento'=>$complemento,
  'numero'=>$numero,
  'bairro'=>$bairro,
  'referencia'=>$referencia      
 ])->where(['id_endereco', $id_endereco])->update();  
 }

 if($insere){
    $_SESSION['msg'] = 'Cliente atualizado com sucesso!';
    $this->redirect(BASE_URL."clientes");
}  else{
    $_SESSION['msg'] = 'Ooops! erro ao atualizar tente novamente!';
    $this->redirect(BASE_URL."clientes");
}
    
endif;
}//add action


 
public function del($id){

if(isset($id) && intval($id)){
$qr = new Orm('clientes');
$deletar = $qr->del(['id', $id]);

$_SESSION['msg'] = "Registro removido com sucesso!";
$this->redirect(BASE_URL."clientes");
}

}//del  


public function getBairros(){
        $id_company  = $this->user->getCompany();
        $bairros = new Orm('bairros');
        $dados['lista_bairros'] = $bairros->select('*')->where(['id_company', $id_company])->get();

        echo json_encode($dados['lista_bairros']);
        exit;
}//getBairros


public function getName(){
     $id_company  = $this->user->getCompany();  
     $nome = $_POST['nome'];
     $clientes = new Orm('clientes');
     $dados['cliente'] = $clientes->select(['ref_user'])->where(['nome', $nome])->and(['id_company', $id_company])->first()->get();


     echo json_encode($dados['cliente']);
     exit;
}//getName


public function getCupons(){
     $dados = array();
     $cliente = $_POST['cliente'];      


     echo json_encode($dados['pega_cupon']);
     exit;     
}//getCupons


}