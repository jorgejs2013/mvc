<?php
use Core\Controller;
use Models\Users;
use Models\Permissao;
use Models\Relatorio;
use Models\Crud;
use Models\Helpers;
use Models\ImportCsv;
use Models\Search;
use Models\Orm;


class ContatoController extends Controller {

	private $user;
  private $arrayInfo;

   public function __construct() {              
        $this->user = new Users();

        if($this->user->isLogged() == false){                  
         header("Location: ".BASE_URL."login");        
         exit;         
        }  

        $usuario = $this->user->getid();
        $alias = 'super-admin';
        $this->permissao = new Permissao();                 
        $permissoes = $this->permissao->getPermissoes($usuario);

        $this->arrayInfo = array(
         'user'=>$this->user,
         'menuActive'=>'home',
         'bread'=>'Inicio',
         'permissoes'=>$permissoes 
        );          
}

public function index() {   
$this->arrayInfo['list_js'] = array(
'jquery.table-shrinker',
'shrinker_init',
'sweetalert2.all.min'
);    

$c = new Orm('contato');  
$h = new Helpers();
$id_company = $this->user->getCompany();

$this->arrayInfo['lista'] = $c->select('*')->get();

if(isset($_SESSION['msg']) && !empty($_SESSION['msg'])){
  $this->arrayInfo['msg'] = $_SESSION['msg'];
  unset($_SESSION['msg']);
}


$this->loadTemplate('contato/listar', $this->arrayInfo);
}//function index   

public function add(){
$this->arrayInfo['list_js'] = array(
'parsley/parsley.min',
'parsley/pt-br',
'tab',
'flatpickr/flatpickr',
'flatpickr/flatpickr_init',
'jquery.mask.min',
'mask_init'
);    

$cid = new Orm('cidade');
$est = new Orm('estado');
$this->arrayInfo['estados'] = $est->select('*')->get();
$this->arrayInfo['cidades'] = $cid->select('*')->get();

if(isset($_SESSION['formError']) && count($_SESSION['formError']) >0){
$array['errorItems'] = $_SESSION['formError'];
unset($_SESSION['formError']);
}

$this->loadTemplate('contato/adicionar', $this->arrayInfo);
}//add

public function add_action(){
$c = new Orm('contato');

if(isset($_POST['nome']) && !empty($_POST['nome'])){

$eh_cliente = (isset($_POST['eh_cliente'])) ? 'S': 'N';
$eh_funcionario = (isset($_POST['eh_funcionario'])) ? 'S': 'N';
$eh_motorista = (isset($_POST['eh_motorista'])) ? 'S': 'N';
$nome = addslashes(trim($_POST['nome']));
$data_nasc = $this->converterData($_POST['data_nascimento'], '/');
$fantasia = addslashes(trim($_POST['fantasia']));
$cpf = addslashes(trim($_POST['cpf']));
$cnpj = addslashes(trim($_POST['cnpj']));
$data_cadastro = date('Y-m-d');
$ddd = addslashes(trim($_POST['ddd']));
$fone = addslashes(trim($_POST['telefone']));
$celular = addslashes(trim($_POST['celular']));
$email = addslashes(trim($_POST['email']));
$senha = addslashes(trim($_POST['senha']));

$cep = addslashes(trim($_POST['cep']));
$logradouro = addslashes(trim($_POST['logradouro']));
$numero = addslashes(trim($_POST['numero']));
$id_estado = (isset($_POST['uf'])) ? addslashes(trim($_POST['uf'])) : null;
$id_cidade = (isset($_POST['cidade'])) ? addslashes(trim($_POST['cidade'])) : null;
$complemento = addslashes(trim($_POST['complemento']));
$bairro = addslashes(trim($_POST['bairro']));

$ie = addslashes(trim($_POST['ie']));
$im = addslashes(trim($_POST['im']));
$rg = addslashes(trim($_POST['rg']));
$suframa = addslashes(trim($_POST['suframa']));
$cod_estrangeiro = addslashes(trim($_POST['cod_estrangeiro']));
$ie_subt_trib = addslashes(trim($_POST['ie_subt_trib']));
$indiedest = (isset($_POST['indiedest'])) ? addslashes(trim($_POST['indiedest'])) : null;

//gravando os dados
$insere = $c->set([
'eh_cliente'=>$eh_cliente,
'eh_funcionario'=>$eh_funcionario,
'eh_motorista'=>$eh_motorista,
'nome'=>$nome,
'data_nasc'=>$data_nasc,
'fantasia'=>$fantasia,
'cpf'=>$cpf,
'cnpj'=>$cnpj,
'data_cadastro'=>$data_cadastro,
'ddd'=>$ddd,
'fone'=>$fone,
'celular'=>$celular,
'email'=>$email,
'senha'=>$senha,
'cep'=>$cep,
'logradouro'=>$logradouro,
'numero'=>$numero,
'id_estado'=>$id_estado,
'id_cidade'=>$id_cidade,
'complemento'=>$complemento,
'bairro'=>$bairro,
'ie'=>$ie,
'im'=>$im,
'rg'=>$rg,
'suframa'=>$suframa,
'cod_estrangeiro'=>$cod_estrangeiro,
'ie_subt_trib'=>$ie_subt_trib
])->save();

$_SESSION['msg'] = "Cadastro realizado com sucesso!";
header("Location: ".BASE_URL."contato");
exit;

}else{
   $_SESSION['formError'] = array('nome','ativo');   
   header("Location: ".BASE_URL."contato/add");
   exit; 
}

}//add action



public function edit($id){
$c = new Orm('contato');
$this->arrayInfo['info'] = $c->select('*')->where(['id_contato', $id])->first()->get();

$this->arrayInfo['id_contato'] = $id;
$this->arrayInfo['list_js'] = array(
'parsley/parsley.min',
'parsley/pt-br',
'tab',
'flatpickr/flatpickr',
'flatpickr/flatpickr_init',
'jquery.mask.min',
'mask_init'
);     

$cid = new Orm('cidade');
$est = new Orm('estado');
$this->arrayInfo['estados'] = $est->select('*')->get();
$this->arrayInfo['cidades'] = $cid->select('*')->get();
$this->arrayInfo['id_contato'] = $id;

if(isset($_SESSION['formError']) && count($_SESSION['formError']) >0){
$array['errorItems'] = $_SESSION['formError'];
unset($_SESSION['formError']);
}


$this->loadTemplate('contato/editar', $this->arrayInfo);
}//edit

public function edit_action($id){
$c = new Orm('contato');

if(isset($_POST['nome']) && !empty($_POST['nome'])){
$eh_cliente = (isset($_POST['eh_cliente'])) ? 'S': 'N';
$eh_funcionario = (isset($_POST['eh_funcionario'])) ? 'S': 'N';
$eh_motorista = (isset($_POST['eh_motorista'])) ? 'S': 'N';
$nome = addslashes(trim($_POST['nome']));
$data_nasc = $this->converterData($_POST['data_nascimento'], '/');
$fantasia = addslashes(trim($_POST['fantasia']));
$cpf = addslashes(trim($_POST['cpf']));
$cnpj = addslashes(trim($_POST['cnpj']));
$ddd = addslashes(trim($_POST['ddd']));
$fone = addslashes(trim($_POST['telefone']));
$celular = addslashes(trim($_POST['celular']));
$email = addslashes(trim($_POST['email']));
$senha = addslashes(trim($_POST['senha']));
$cep = addslashes(trim($_POST['cep']));
$logradouro = addslashes(trim($_POST['logradouro']));
$numero = addslashes(trim($_POST['numero']));
$id_estado = (isset($_POST['uf'])) ? addslashes(trim($_POST['uf'])) : null;
$id_cidade = (isset($_POST['cidade'])) ? addslashes(trim($_POST['cidade'])) : null;
$complemento = addslashes(trim($_POST['complemento']));
$bairro = addslashes(trim($_POST['bairro']));

$ie = addslashes(trim($_POST['ie']));
$im = addslashes(trim($_POST['im']));
$rg = addslashes(trim($_POST['rg']));
$suframa = addslashes(trim($_POST['suframa']));
$cod_estrangeiro = addslashes(trim($_POST['cod_estrangeiro']));
$ie_subt_trib = addslashes(trim($_POST['ie_subt_trib']));

$up = $c->set([
'eh_cliente'=>$eh_cliente,
'eh_funcionario'=>$eh_funcionario,
'eh_motorista'=>$eh_motorista,
'nome'=>$nome,
'data_nasc'=>$data_nasc,
'fantasia'=>$fantasia,
'cpf'=>$cpf,
'cnpj'=>$cnpj,
'ddd'=>$ddd,
'fone'=>$fone,
'celular'=>$celular,
'email'=>$email,
'senha'=>$senha,
'cep'=>$cep,
'logradouro'=>$logradouro,
'numero'=>$numero,
'id_estado'=>$id_estado,
'id_cidade'=>$id_cidade,
'complemento'=>$complemento,
'bairro'=>$bairro,
'ie'=>$ie,
'im'=>$im,
'rg'=>$rg,
'suframa'=>$suframa,
'cod_estrangeiro'=>$cod_estrangeiro,
'ie_subt_trib'=>$ie_subt_trib
])->where(['id_contato', $id])->update();

$_SESSION['msg'] = "Cadastro atualizado com sucesso!";
header("Location: ".BASE_URL."contato");
exit;

}else{
   $_SESSION['formError'] = array('nome','ativo');   
   header("Location: ".BASE_URL."contato/add");
   exit; 
}
}//edit action


public function importar(){
$c = new Crud(); 
$id_company  = $this->user->getCompany();

$this->arrayInfo['list_js'] = array(
'import_csv_alunos',
'sweetalert2.all.min'
);

if(isset($_SESSION['msg']) && !empty($_SESSION['msg'])){
  $this->arrayInfo['msg'] = $_SESSION['msg'];
  unset($_SESSION['msg']);  
}

if(isset($_FILES['file']) && !empty($_FILES['file']['tmp_name'])){
$csv = $_FILES['file']['tmp_name'];
$file = fopen($csv, 'r');

//adicionar essa linha abaixo para pular a primeira linha
fgetcsv($file);

$i = 0;
while(($row = fgetcsv($file,1000,';')) !== FALSE){

 $consulta = $c->listaPorCampo('contato', "nome='{$row[0]}'");

 if(empty($consulta)){ 

 $form = new \stdClass();
 $form->id_company = $id_company;
 $form->eh_cliente = 'S';
 $form->nome = utf8_encode($row[0]); 
 $form->data_nasc = $row[1];
 $form->cpf = $row[2];
 $form->data_cadastro = date('Y-m-d');
 $form->ddd = $row[3];
 $form->fone = $row[4];
 $form->celular = $row[5];
 $form->email = $row[6];
 $form->cep = $row[7];
 $form->logradouro = $row[8];
 $form->numero = $row[9];
 $form->complemento = $row[10];
 $form->bairro = $row[11];

 $grv =  $c->inserir('contato', $form);
  if($grv){
   $i++;  
  }
  
}//se caso o item n tiver no banco de dados

}//fim do while

if($i >0){

 $_SESSION['msg'] = "Contatos importados com sucesso!";
 sleep(1);
 fclose($file);
 header("Location:".BASE_URL."contato/importar");
 exit;

}else{

  $_SESSION['msg'] = "Não há contatos para importar";
 sleep(1);
 fclose($file);
 header("Location:".BASE_URL."contato/importar");
 exit;
}

}
$this->loadTemplate('contato/importar', $this->arrayInfo);
}//importar


public function import(){
$this->arrayInfo['list_js'] = array(
'import_csv_alunos',
); 


$this->loadTemplate('contato/importar', $this->arrayInfo);
}//import

public function upload_csv(){
$error = '';
$total_line = '';


if($_FILES['file']['name'] != ''){
$allowed_extensions = array('csv');
$file_array = explode('.', $_FILES['file']['name']);
$extension = end($file_array);

if(in_array($extension, $allowed_extensions)){

$new_file_name = rand().'.'.$extension;
$_SESSION['csv_file_name'] = $new_file_name;
$nomeArquivo = (string) "file".$new_file_name;
$temp = $_FILES['file']['tmp_name'];


if(move_uploaded_file($temp, "file/".$nomeArquivo))
{
    $file_content = file('file/'.$nomeArquivo, FILE_SKIP_EMPTY_LINES);
    $total_line = COUNT($file_content);
}else{
 $error = "Erro ao enviar arquivo";  
}

}else{
  $error = 'so é permitod arquivo do formato CSV';
}
}else{
$error = 'Por favor selecione um arquivo!';
}

if($error != ''){
  $output = array(
  'error'=> $error
);
}else{
  $output = array(
   'success'=>true,
   'total_line'=> ($total_line -1)
  );
}

echo json_encode($output);
exit;
}//upload_csv

public function import_csv(){
header("Content-type:text/html;charset=utf-8");
header("Content-Control: no-cache, must-revalidate");
header("Pragma:no-cache");

$c = new Crud();
$import = new ImportCsv();

set_time_limit(0);
ob_implicit_flush(1);

if(isset($_SESSION['csv_file_name']))
{
  //Nome do arquivo salvo dentro da pasta
  $nome_arquivo = "file".$_SESSION['csv_file_name'];

  //$import->setFile($nome_arquivo, true);
  // foreach($import->getData() as $data) {

  //   echo "<pre>";
  //   print_r($data);
  //   exit;
  // }

  $file = fopen('file/'.$nome_arquivo, "r");
  while (($getData = fgetcsv($file, 10000, ",")) !== FALSE){
    echo $getData[0]."<br>";
    echo $getData[1];
    exit;
    echo "<pre>";
    print_r($getData);
    exit;
  }


  echo "<br>parou<br>";exit;
  
  $file_data = fopen('file/'.$nome_arquivo, 'r');
  fgetcsv($file_data);
  while($row = fgetcsv($file_data)){  


   echo "<pre>";
   print_r($row);
   exit;

   $form = new \stdClass();
   $form->nome  = $row[0];
   $form->data_nasc = $row[1];
   $form->cpf = $row[2];
   $form->ddd = $row[3];
   $form->fone = $row[4];
   $form->celular = $row[5];
   $form->email = $row[6];
   $form->nome_resp = $row[7];
   $form->contato_resp = $row[8];
   $form->data_nasc_resp = $row[9];
   $form->email_resp = $row[10];
   $form->cep = $row[11];
   $form->logradouro = $row[12];
   $form->numero = $row[13];
   $form->complemento = $row[14];
   $form->bairro = $row[15];

   echo "<pre>";
   print_r($form);
   exit;

   $c->inserir("contato", $form);
    
    sleep(1);

    if(ob_get_level() >0){
      ob_end_flush();
    }
  }//fim do while
  unset($_SESSION['csv_file_name']);
}
}//import csv

public function process_csv(){
$c  = new Crud(); 
$itens = $c->lista('contato', "eh_cliente='S'");

echo COUNT($itens);
}//process_csv


public function del($id){
$c = new Orm('contato');
$c->del(['id_contato', $id]);

$_SESSION['msg'] = "Contato removido com sucesso!";
header("Location: ".BASE_URL."contato");
exit; 
}//del

}