<?php
use Core\Controller;
use Models\Users;
use Models\Permissao;
use Models\Helpers;
use Models\Orm;
use Models\Search;
use Models\Atendimento;

class PesquisarController extends Controller {

	private $user;
  private $arr;

   public function __construct() {              
        $this->user = new Users();

        if($this->user->isLogged() == false){                  
         header("Location: ".BASE_URL."login");        
         exit;         
        }  

        $usuario = $this->user->getid();
        $alias = 'super-admin';
        $this->permissao = new Permissao();                 
        $permissoes = $this->permissao->getPermissoes($usuario);

        $this->arr = array(
         'user'=>$this->user,
         'menuActive'=>'Pesquisr',
         'bread'=>'Pesquis',
         'permissoes'=>$permissoes 
        );          
}

public function index() {   
$this->arr['list_js'] = array(
'sweetalert2.all.min',
'parsley/parsley.min',
'parsley/pt-br',
'jquery.mask.min',
'mask_init',
);    

$this->arr['grupos'] = (new Orm('grupos'))->select('*')->get();
$this->arr['empresas'] = (new Orm('unidade'))->select('*')->get();
$this->arr['motoristas'] = (new Orm('veiculos'))->select('*')->get();

if(isset($_SESSION['msg']) && !empty($_SESSION['msg'])){
  $this->arrayInfo['msg'] = $_SESSION['msg'];
  unset($_SESSION['msg']);
}

$this->loadTemplate('pesquisar/home', $this->arr);
}//function index   


public function consulta(){
$this->arr['list_js'] = array(
'sweetalert2.all.min',
'parsley/parsley.min',
'parsley/pt-br',
'jquery.mask.min',
'mask_init',
'confirm_atendimento',
'manager_atendimento'
);

$this->arr['grupos'] = (new Orm('grupos'))->select('*')->get();
$this->arr['empresas'] = (new Orm('unidade'))->select('*')->get();
$this->arr['motoristas'] = (new Orm('veiculos'))->select('*')->get();

$s = new Search();

$filter = array('voucher'=>'','grupo'=>'','empresa'=>'','motorista'=>'','data_ini'=>'', 'data_fim'=>'');

if(isset($_POST['empresa']) || isset($_POST['num_voucher']) || isset($_POST['grupo']) || isset($_POST['motorista'])){

$num_voucher = $this->limpaCampo($_POST['num_voucher']);
$grupo = $this->limpaCampo($_POST['grupo']);
$empresa = $this->limpaCampo($_POST['empresa']);
$motorista = $this->limpaCampo($_POST['motorista']);
$data_ini = $this->limpaCampo($_POST['data_ini']);
$data_fim = $this->limpaCampo($_POST['data_fim']);

if($num_voucher != null){  $filter['voucher'] = $num_voucher;}
if($grupo != null){  $filter['grupo'] = $grupo;}
if($empresa != null){  $filter['empresa'] = $empresa;}
if($motorista != null){  $filter['motorista'] = $motorista;}
if($data_ini != null){  $filter['data_ini'] = $this->converterData($data_ini,'/');}
if($data_fim != null){  $filter['data_fim'] = $this->converterData($data_fim,'/');}

$this->arr['dados'] = $s->getAllSearch('atendimentos', $filter, null);
$a = new Atendimento();
$this->arr['list'] = $a->atendimentos($this->arr['dados']);

//echo $this->debug($this->arr['list']);

$this->loadTemplate('pesquisar/home', $this->arr);
}

}//consulta



public function consultaRelatorio(){
$this->arr['list_js'] = array(
'sweetalert2.all.min',
'parsley/parsley.min',
'parsley/pt-br',
'jquery.mask.min',
'mask_init',
'confirm_atendimento',
'manager_atendimento'
);
  
$this->arr['usuario_logado'] = $this->user->getName();   
$this->arr['grupos'] = (new Orm('grupos'))->select('*')->get();
$this->arr['empresas'] = (new Orm('unidade'))->select('*')->get();
$this->arr['motoristas'] = (new Orm('veiculos'))->select('*')->get();

if(isset($_POST['empresa']) || isset($_POST['grupo']) || isset($_POST['motorista'])){

$tipo_pesquisa = $this->limpaCampo($_POST['tipo_pesquisa']);
$grupo = $this->limpaCampo($_POST['grupo']);
$empresa = $this->limpaCampo($_POST['empresa']);
$motorista = $this->limpaCampo($_POST['motorista']);
$data_ini = $this->limpaCampo($_POST['data_ini']);
$data_fim = $this->limpaCampo($_POST['data_fim']);

if($data_ini != null){  $filter['data_ini'] = $this->converterData($data_ini,'/');}
if($data_fim != null){  $filter['data_fim'] = $this->converterData($data_fim,'/');}

$a = new Atendimento();
$this->arr['list'] = $a->buscaRelatorio($tipo_pesquisa, $dados = array('id_grupo'=>$grupo,'id_empresa'=>$empresa,'veiculo'=>$motorista, 'data_ini'=>$data_ini, 'data_fim'=>$data_fim));

$this->arr['list2'] = $a->buscaRelatorio2($tipo_pesquisa, $dados = array('id_grupo'=>$grupo,'id_empresa'=>$empresa,'veiculo'=>$motorista, 'data_ini'=>$data_ini, 'data_fim'=>$data_fim));

//echo $this->debug($this->arr['list']);

$this->loadTemplate('pesquisar/atendimentos', $this->arr);
}

}//consultaRelatorio

}