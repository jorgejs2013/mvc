<?php
use Core\Controller;
use Models\Users;
use Models\Permissao;
use Models\Orm;
use Models\Search;

class CategoriasController extends controller {

private $user;
private $arr;

 public function __construct() {              
        $this->user = new Users();

        if($this->user->isLogged() == false){                  
         header("Location: ".BASE_URL."login");        
         exit;         
        }


        $this->arr = array(
         'user'=>$this->user,
         'menuActive'=>'cadastros',
         'bread'=>'Categorias'        
        );          
}

public function index() {
$this->arr['list_js'] = array(
'plugins/jquery.table-shrinker',
'shrinker_init', 
'plugins/jquery.mask.min',
'mask_init',
'plugins/sweetalert2.all.min'
);     

$id_company  = $this->user->getCompany();        
$categoria = new Orm('categorias');


//Filtrando pesquisas
$dados['filter'] = array('nome'=>'');
if(isset($_GET['nome']) && !empty($_GET['nome'])){
$dados['filter']['nome_categoria'] = $_GET['nome'];

$s = new Search();
$this->arr['lista'] = $s->getAllSearch('categorias', $dados['filter']);
}else{
$this->arr['lista'] = $categoria->select('*')->where(['id_company', $id_company])->paginate('10')->get();
$this->arr['paginacao'] = $categoria->render('categorias'); 
}
 

$this->arr['msg'] = $this->flashMessage($_SESSION['msg'] ?? null);

$this->loadTemplate('categorias/listar', $this->arr);
}//index


public function add(){
$this->arr['list_js'] = array(
'plugins/jquery.mask.min',
'mask_init',
'plugins/parsley/parsley.min',
'plugins/parsley/pt-br',
'preview_imagem'
);   

$id_company  = $this->user->getCompany();   

$this->loadTemplate('categorias/adicionar', $this->arr);
}//add


public function add_action($id){
$id_company  = $this->user->getCompany();  


if(isset($_POST['nome']) && !empty($_POST['nome'])):
$nome = $this->limpaCampo($_POST['nome']);


$img_categoria = NULL;
if($_FILES['imagem_categoria']['name'] != ''){ 
    if($this->imagemValida($_FILES['imagem_categoria'])){       
       $img_categoria =  $this->gravarImagem($_FILES['imagem_categoria'], array(680,400), 'medias/categorias');
    }else{
      $_SESSION['msg'] = "Erro ao salvar imagem tente novamente, apenas arquivos com tamanho menor a 2MB!";
      header("Location:".BASE_URL."categorias/add");
      exit;
   }
}

$icon_categoria = NULL;
if($_FILES['image_icon']['name'] != ''){ 
    if($this->imagemValida($_FILES['image_icon'])){       
       $icon_categoria =  $this->gravarImagem($_FILES['image_icon'], array(680,400), 'medias/categorias');
    }else{
      $_SESSION['msg'] = "Erro ao salvar imagem tente novamente, apenas arquivos com tamanho menor a 2MB!";
      header("Location:".BASE_URL."categorias/add");
      exit;
   }
}

     
$categoria = new Orm('categorias');
$insere = $categoria->set([
'id_company'=>$id_company,         
'nome_categoria'=>$nome, 
'img_categoria'=>$img_categoria,
'icon_categoria'=>$icon_categoria
])->save();         


 if($insere){
    $_SESSION['msg'] = 'Categoria cadastrada com sucesso!';
    $this->redirect(BASE_URL."categorias");
}  else{
    $_SESSION['msg'] = 'Ooops! erro ao cadastrar novamente!';
    $this->redirect(BASE_URL."categorias");
}

    
endif;
}//add action

public function edit($id){
$this->arr['list_js'] = array(
'plugins/jquery.mask.min',
'mask_init',
'plugins/parsley/parsley.min',
'plugins/parsley/pt-br',
'preview_imagem'
); 

$id_company  = $this->user->getCompany();

$categoria = new Orm('categorias');
$this->arr['info'] = $categoria->select('*')->where(['id_categoria', $id])->e(['id_company', $id_company])->first()->get();

$this->arr['id_categoria'] = $id;

$this->loadTemplate('categorias/editar', $this->arr);
}//edit



public function edit_action($id){
$id_company  = $this->user->getCompany();  


$categoria = new Orm('categorias');
$cat = $categoria->select('*')->where(['id_categoria', $id])->e(['id_company', $id_company])->first()->get();

if(isset($_POST['nome']) && !empty($_POST['nome'])):
$nome = $this->limpaCampo($_POST['nome']);
       
$img_categoria = $cat->img_categoria;
if($_FILES['imagem_categoria']['name'] != ''){ 
    if($this->imagemValida($_FILES['imagem_categoria'])){       
       $img_categoria =  $this->gravarImagem($_FILES['imagem_categoria'], array(680,400), 'medias/categorias');
    }else{
      $_SESSION['msg'] = "Erro ao salvar imagem tente novamente, apenas arquivos com tamanho menor a 2MB!";
      header("Location:".BASE_URL."categorias/add");
      exit;
   }
}

$icon_categoria = $cat->icon_categoria;
if($_FILES['image_icon']['name'] != ''){ 
    if($this->imagemValida($_FILES['image_icon'])){       
       $icon_categoria =  $this->gravarImagem($_FILES['image_icon'], array(680,400), 'medias/categorias');
    }else{
      $_SESSION['msg'] = "Erro ao salvar imagem tente novamente, apenas arquivos com tamanho menor a 2MB!";
      header("Location:".BASE_URL."categorias/add");
      exit;
   }
}        


$categoria = new Orm('categorias');
$insere = $categoria->set([
'id_company'=>$id_company,         
'nome_categoria'=>$nome, 
'img_categoria'=>$img_categoria,
'icon_categoria'=>$icon_categoria
])->where(['id_categoria', $id])->update();  

 if($insere){
    $_SESSION['msg'] = 'Categoria atualizada com sucesso!';
    $this->redirect(BASE_URL."categorias");
}  else{
    $_SESSION['msg'] = 'Ooops! erro ao atualizar tente novamente!';
    $this->redirect(BASE_URL."categorias");
}
    
endif;

}//edit action


 
public function del($id){

if(isset($id) && intval($id)){
$qr = new Orm('categorias');
$deletar = $qr->del(['id_categoria', $id]);

$_SESSION['msg'] = "Registro removido com sucesso!";
$this->redirect(BASE_URL."categorias");
}

}//del  

}