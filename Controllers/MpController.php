<?php
use Core\Controller;
use Models\Users;
use Models\Permissao;
use Models\Crud;
use Models\Helpers;
use Models\Orm;

class MpController extends Controller {

	private $user;
  private $arrayInfo;

   public function __construct() {              
        $this->user = new Users();

        if($this->user->isLogged() == false){                  
         header("Location: ".BASE_URL."login");        
         exit;         
        }  

        $usuario = $this->user->getid();
        $alias = 'super-admin';
        $this->permissao = new Permissao();                 
        $permissoes = $this->permissao->getPermissoes($usuario);

        $this->arrayInfo = array(
         'user'=>$this->user,
         'menuActive'=>'Departamentos',
         'bread'=>'Departamentos',
         'permissoes'=>$permissoes 
        );          
}


public function processar(){
$this->arrayInfo['list_js'] = array(
'parsley/parsley.min',
'parsley/pt-br',
'jquery.mask.min',
'mask_init',
'select_unidade'
);    

//recebe o posto do  modal com os dados
$mp = $_POST;

MercadoPago\SDK::setAccessToken("TEST-8275680737687645-010208-3ad5f1c0a831e39bbb7c97540b82401d__LD_LB__-98144477");

$payment = new MercadoPago\Payment();
$payment->transaction_amount = (float)$mp['transactionAmount'];
$payment->token = $mp['token'];
$payment->description = "descrição da venda";
$payment->installments = (int)$mp['installments'];
$payment->payment_method_id = $mp['paymentMethodId'];
$payment->issuer_id = (int)$mp['issuerId'];

$payer = new MercadoPago\Payer();
$payer->email = "jorge_nanova@hotmail.com";
$payer->identification = array(
    "type" => $mp['identificationType'],
    "number" => $mp['identificationNumber']
);

$payment->payer = $payer;
$ref = "MP".time().rand(0,99999);
$payment->external_reference = $ref;
$payment->save();

if($payment->status ==  "approved"){
 //gravar no banco de dados
    echo "disparar função para gravar no banco <br>";
}

$saida = '';

if(isset($payment->error->status) && $payment->error->status == '400'){
  $erro = $payment->error->causes[0]->description;

  $saida .=  "<div class='alert-danger' role='alert' style='width: 580px;'>";
  $saida .=  "<h3>Ooops! Houve uma falha. ".$erro."</h3>";
  $saida .= "<p>Para visualizar os sorteios <a href='".BASE_URL."'>clique aqui para voltar</a></p>";
  $saida .=  "</div>";  
}



if($payment->status == 'approved'){

//Swithc para verificar o status do pagamento
switch($payment->status_detail) {
 case "accredited":
    $retorno = "Aprovado";
 break;

 case "pending_contingency":
    $retorno = "Estamos processando o pagamento. Em até 2 dias úteis informaremos por e-mail o resultado.";
 break;

 case "pending_review_manual":
    $retorno = "Estamos processando o pagamento. Em até 2 dias úteis informaremos por e-mail se foi aprovado ou se precisamos de mais informações.";
  break;

   case "cc_rejected_bad_filled_card_number":
        $retorno = "Confira o número do cartão.";
   break;

   case "cc_rejected_bad_filled_date":
     $retorno = "Confira a data de validade.";
   break;

   case "cc_rejected_bad_filled_other":
     $retorno = "Confira os dados.";
   break;

   case "cc_rejected_bad_filled_security_code":
        $retorno = "Confira o código de segurança.";
    break;

   case "cc_rejected_blacklist":
        $retorno = "Não conseguimos processar seu pagamento.";
    break;

    case "cc_rejected_call_for_authorize":
         $retorno = "Você deve autorizar em seu banco o pagamento do valor ao Mercado Pago.";
    break;

    case "cc_rejected_card_disabled":
         $retorno = "Ligue para o payment_method_id para ativar seu cartão. O telefone está no verso do seu cartão.";
    break;

    case "cc_rejected_card_error":
         $retorno = "Não conseguimos processar seu pagamento. Tente novamente mais tarde.";
    break;

    case "cc_rejected_duplicated_payment":
         $retorno = "Você já efetuou um pagamento com esse valor. Caso precise pagar novamente, utilize outro cartão ou outra forma de pagamento.";
    break;

    case "cc_rejected_high_risk":
      $retorno = "Seu pagamento foi recusado. Escolha outra forma de pagamento. Recomendamos meios de pagamento por boleto.";
    break;

    case "cc_rejected_insufficient_amount":
        $retorno = "O seu cartão possui saldo insuficiente. Tente outro cartão ou selecione a opção boleto.";
    break;

    case "cc_rejected_invalid_installments":
        $retorno = "O seu cartão não processa pagamentos parcelados.";
    break;

    case "cc_rejected_max_attempts":
        $retorno = "Você atingiu o limite de tentativas permitido. Escolha outro cartão ou outra forma de pagamento.";
    break;

    case "cc_rejected_other_reason":
         $retorno = "O seu banco não conseguiu processar o seu pagamento. Tente novamente em instantes.";
    break;
}


if($payment->status_detail == 'accredited'){
$saida .= "<div class='alert-aproved' style='width: 580px;'>";
$saida .= "<div class='card'>";
$saida .= "<h4 class='card-header text-center bg-success text-white'>Sucesso!</h4>";
$saida .= "<div class='card-body'>";
$saida .= "<div class='text-center'>";

$saida .= "</div>";
$saida .= "<h5 class='card-title'>Obrigado!</h5>";
$saida .= "<p>Seu pedido foi recebido com sucesso!</p>";
$saida .= "<p>Status: ".$retorno."</p>";
$saida .= "</br></br></br></br>";
$saida .= "<a class='btn btn-success' href='".BASE_URL."' role='button'>voltar para o site</a>";
$saida .= "</div>";
$saida .= "</div>";
$saida .= "</div>";


}else if($payment->status_detail == "pending_contingency"){// se estiver em processamento exibe abaixo
$saida .= "<div class='alert-processando' style='width: 580px;'>";
$saida .= "<div class='card'>";
$saida .= "<h4 class='card-header text-center bg-alert text-white'>Em processamento!</h4>";
$saida .= "<div class='card-body'>";
$saida .= "<div class='text-center'>";
$saida .= "<img class='imagem_ic' src='image/alert.png' alt='erro'/>";
$saida .= "</div>";
$saida .= "<h5 class='card-title'>Obrigado!</h5>";
$saida .= "<p>Seu pagamento está em processamento, poderá levar até 48h para analisar!</p>";
$saida .= "<p>Status: ".$retorno."</p>";
$saida .= "</br></br></br></br>";
$saida .= '<a class="btn btn-success" href="index.php" role="button">voltar para o site</a>';
$saida .= "</div>";
$saida .= "</div>";
$saida .= "</div>";

}else{

$saida .= "<div class='card-falha' style='width: 580px;'>";
$saida .= "<div class='card'>";
$saida .= "<h4 class='card-header bg-danger text-white'>";
$saida .= "Ooops! Não foi possível prosseguir :/<br>";
$saida .= "</h4>";
$saida .= "<div class='card-body'>";
$saida .= "<p>Houve uma falha.</p>";
$saida .= "</div>";
$saida .= "</div>";

}

}//se status for igual a aprovado


$this->arrayInfo['resposta'] = $saida;

echo $this->debug($this->arrayInfo['resposta']);


$this->loadTemplate('centrocusto/adicionar', $this->arrayInfo);
}//processar




}