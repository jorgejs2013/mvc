<?php
use Core\Controller;
use Models\Users;
use Models\Permissao;
use Models\Orm;

class configController extends controller {

private $user;
private $arr;

 public function __construct() {              
        $this->user = new Users();

        if($this->user->isLogged() == false){                  
         header("Location: ".BASE_URL."login");        
         exit;         
        }


        $this->arr = array(
         'user'=>$this->user,
         'menuActive'=>'configuracoes',
         'bread'=>'Config'        
        );          
}

public function index() {
$this->arr['list_js'] = array(
'plugins/jquery.table-shrinker',
'shrinker_init', 
'plugins/jquery.mask.min',
'plugins/parsley/parsley.min',
'plugins/parsley/pt-br',
'mask_init',
'preview_imagem',
'plugins/sweetalert2.all.min',
'summernote-lite.min',
'summernote_init'
);     
 
$this->arr['id_config'] = 1;
$this->arr['info'] = (new Orm('config'))->select('*')->where(['id_config', 1])->first()->get();
$this->arr['msg'] = $this->flashMessage($_SESSION['msg'] ?? null);

$this->loadTemplate('config/config', $this->arr);
}//index



public function add_action($id){

if(isset($_POST['nome']) && !empty($_POST['nome'])):
$titulo = $this->limpaCampo($_POST['nome']);
$endereco = $this->limpaCampo($_POST['endereco']); 
$numero_contato = $this->limpaCampo($_POST['numero_contato']);
$numero_zap = $this->limpaCampo($_POST['numero_zap']);
$pedido_minimo = $this->limpaCampo($_POST['pedido_minimo']);
$comissao_bonus = $this->limpaCampo($_POST['comissao_bonus']);

$termos = $_POST['termos'];
$politicas = $_POST['politicas'];
$sobre = $_POST['sobre'];
$faq = $_POST['faq'];


$logomarca = NULL;
if($_FILES['logomarca']['name'] != ''){ 
    if($this->imagemValida($_FILES['logomarca'] ?? NULL)){       
       $logomarca =  $this->gravarImagem($_FILES['logomarca'], array(680,400), 'medias/config');
    }else{
      $_SESSION['msg'] = "Erro ao salvar imagem tente novamente, apenas arquivos com tamanho menor a 2MB!";
      header("Location:".BASE_URL."config");
      exit;
   }
}

$favicon = NULL;
if($_FILES['favicon']['name'] != ''){ 
    if($this->imagemValida($_FILES['favicon'] ?? NULL)){       
       $logomarca =  $this->gravarImagem($_FILES['favicon'], array(250,100), 'medias/config');
    }else{
      $_SESSION['msg'] = "Erro ao salvar imagem tente novamente, apenas arquivos com tamanho menor a 2MB!";
      header("Location:".BASE_URL."config");
      exit;
   }
}




$tem = (new Orm("config"))->select('*')->where(['id_config', $id])->first()->get();

if(empty($tem)){
$config = new Orm('config');
$insere = $config->set([
'titulo'=>$titulo,         
'endereco'=>$endereco, 
'numero_contato'=>$numero_contato,        
'numero_zap'=>$numero_zap, 
'valor_comissao'=>$comissao_bonus,         
'pedido_minimo'=>$pedido_minimo,
'validade_bonus'=>$comissao_bonus,
'logomarca'=>$logomarca, 
'favicon'=>$favicon,
'termos'=>$termos,
'politica'=>$politicas,
'sobre'=>$sobre,
'faq'=>$faq
])->save();

}else{

$config = new Orm('config');
$insere = $config->set([
'titulo'=>$titulo,         
'endereco'=>$endereco, 
'numero_contato'=>$numero_contato,        
'numero_zap'=>$numero_zap, 
'valor_comissao'=>$comissao_bonus,         
'pedido_minimo'=>$pedido_minimo,
'validade_bonus'=>$comissao_bonus,
'logomarca'=>$logomarca, 
'favicon'=>$favicon,
'termos'=>$termos,
'politica'=>$politicas,
'sobre'=>$sobre,
'faq'=>$faq
])->where(['id_config', $id])->update();

}

 if($insere){
    $_SESSION['msg'] = 'Configurações atualizadas com sucesso!!!';
    $this->redirect(BASE_URL."config");
}  else{
    $_SESSION['msg'] = 'Ooops! erro ao cadastrar novamente!';
    $this->redirect(BASE_URL."config");
}
    
endif;
}//add action


}