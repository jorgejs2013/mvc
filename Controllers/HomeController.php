<?php
use Core\Controller;
use Models\Users;
use Models\Permissao;
use Models\Orm;

class HomeController extends Controller {

  private $user;
  private $arr;

    public function __construct() {              
        $this->user = new Users();

        if($this->user->isLogged() == false){                  
         header("Location: ".BASE_URL."login");        
         exit;         
        }


        $this->arr = array(
         'user'=>$this->user,
         'menuActive'=>'home',
         'bread'=>'Inicio'        
        );          
}

public function index() {   
$this->arr['list_js'] = array(
'plugins/sweetalert2.all.min',
'dash'
);  



if(isset($_SESSEION['msg']) && !empty($_SESSEION['msg'])){
    $this->arr['msg'] = $_SESSEION['msg'];
    unset($_SESSEION['msg']);
}


$this->loadTemplate('home', $this->arr);
}//function index   


}