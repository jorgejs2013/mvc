<?php
use Core\Controller;
use Models\Users;
use Models\Permissao;
use Models\Orm;
use Models\Helpers;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;


class ImportController extends Controller {

  private $user;
  private $arr;

    public function __construct() {              
        $this->user = new Users();

        if($this->user->isLogged() == false){                  
         header("Location: ".BASE_URL."login");        
         exit;         
        }  

        $this->arr = array(
         'user'=>$this->user,
         'menuActive'=>'Import',
         'bread'=>'Inicio'
      
        );          
}


public function motoristas(){
$this->arr['list_js'] = array(
'import_csv',
'sweetalert2.all.min'
);  

$m = new Orm('motorista');

if(isset($_FILES['import_motorista']) && $_FILES['import_motorista']['name'] != ''){

$permitidas = array('xls', 'csv', 'xlsx');
$file_array = explode('.', $_FILES['import_motorista']['name']);
$file_extension = end($file_array);


if(in_array($file_extension, $permitidas)){

$file_name = time().'.'.$file_extension;
move_uploaded_file($_FILES['import_motorista']['tmp_name'], $file_name);

$file_type = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file_name);
$reader    = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($file_type);

$spreadsheet = $reader->load($file_name);
unlink($file_name);
$data = $spreadsheet->getActivesheet()->toArray();
$saida = array();

$acertos = 0;
$errors = 0;
foreach($data as $key=> $row){

if($key >0){
$grava = $m->set([
'cpf'=>$row[0],
'rg'=>$row[1],
'nome'=>$row[2],
'bairro'=>$row[3],
'cidade'=>$this->getCidade($row[4]),
'estado'=>$this->getUf($row[5]),
'cep'=>$row[6],
'telefone1'=>$row[7],
'cnh'=>$row[8],
'categoria'=>$row[9],
'validade'=>$row[10],
'placa'=>$row[11],
'modelo'=>$row[12]
])->save();

if($grava){
  $acertos += 1;
}else{
  $errors += 1;
}

}

}//foreach
echo "importados<br>";
echo "acertos:".$acertos."<br>";
echo "erros :".$errors;
exit;

}else{
  $this->arr['msg'] = 'Apenas xlx, csv ou xlsx tipos permitidos';  
}


}//se tve posto do arquivo de excel


$this->loadTemplate('importar/motoristas', $this->arr);
}//motoristas

public function getUf($estado){
$c = (new Orm('estado'))->select(['id_estado'])->where(['uf_estado', $estado])->first()->get();
return $c->id_estado ?? NULL;
}//getCidade

public function getCidade($cidade){
    
$c = (new Orm('cidade'))->select(['id_cidade'])->where(['nome_cidade', $cidade])->first()->get();

return $c->id_cidade ?? NULL;
}//getCidade

}