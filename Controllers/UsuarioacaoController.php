<?php
use Core\Controller;
use Models\Users;
use Models\Helpers;
use Models\Permissao;
use Models\Orm;

class UsuarioacaoController extends controller {

	private $user;
    private $arrayInfo;

    public function __construct() {        
        $this->user = new Users();
        
        if($this->user->isLogged() == false){                  
         header("Location: ".BASE_URL."login");        
         exit;         
        }

      $usuario = $this->user->getid();
      $this->permissao = new Permissao();        
      $this->permissao->temPermissao($usuario, 'super-admin', 'listar'); 
      $permissoes = $this->permissao->getPermissoes($usuario);

       $this->arrayInfo = array(
      'user'=>$this->user,
      'menuActive'=>'contato',
      'bread'=>'Contato',
       'permissoes'=>$permissoes 
     );  

      $this->permissao = new Permissao();        
      $this->permissao->temPermissao($usuario, 'super-admin', 'listar');  
       
    }//Construtor

public function index() {        
$this->arrayInfo['list_js'] = array(
'jquery.mask.min',
'mask_init',
'sweetalert2.all.min'
);


if(isset($_SESSION['msg']) && !empty($_SESSION['msg'])){
  $this->arrayInfo['msg'] = $_SESSION['msg'];
  unset($_SESSION['msg']);
}

$this->arrayInfo['list'] = (new Orm('usuario_acao'))->select('*')->get();

$this->loadTemplate('permissoes/acao', $this->arrayInfo);
}//index 

public function add(){
$this->arrayInfo['list_js'] = array(
'jquery.mask.min',
'mask_init'
);

$h = new Helpers();

if(isset($_POST['nome']) && !empty($_POST['nome'])):           
$nome = addslashes(trim($_POST['nome']));            

$insere = (new Orm('usuario_acao'))->set(['acao'=>$nome,'alias_acao'=>$h->Name($nome)])->save();

$_SESSION['msg'] = "Ação cadastrada com sucesso!";
header("Location:".BASE_URL."usuarioacao");
exit;
endif;

   $this->loadTemplate('permissoes/acao_add', $this->arrayInfo);
}//add

public function edit($id){
$this->arrayInfo['list_js'] = array(
'jquery.mask.min',
'mask_init'
);

 $h = new Helpers();

$this->arrayInfo['info'] = (new Orm('usuario_acao'))->select('*')->where(['id_acao', $id])->first()->get();

if(isset($_POST['nome']) && !empty($_POST['nome'])):           
$nome = addslashes(trim($_POST['nome']));
$slug = $h->Name($nome);   

$atualiza = (new Orm('usuario_acao'))->set(['acao'=>$nome, 'alias_acao'=>$slug])->where(['id_acao', $id])->update();
$_SESSION['msg'] = 'Ação Atualizada com sucesso!'; 
header("Location:".BASE_URL."usuarioacao");
endif;

     $this->loadTemplate('permissoes/acao_edit', $this->arrayInfo);
}//edit


public function del($id){
$deletar = (new Orm('usuario_acao'))->del(['id_acao', $id]);
header("Location: ".BASE_URL."usuarioacao");
exit;
}//del 


}