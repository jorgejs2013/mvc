<?php
use Core\Controller;
use Models\Users;
use Models\Permissao;
use Models\Orm;
use Models\Search;
use Models\Atendimento;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

class RelatoriosController extends Controller {

private $user;
private $arr;

public function __construct() {              
        $this->user = new Users();

if($this->user->isLogged() == false){                  
 header("Location: ".BASE_URL."login");        
 exit;         
}  
$usuario = $this->user->getid();        
$this->permissao = new Permissao();          
$permissoes = $this->permissao->getPermissoes($usuario);

$this->arr = array(
 'user'=>$this->user,
 'menuActive'=>'relatorios',
 'bread'=>'Relatorios',
 'permissoes'=>$permissoes 
);          
}


public function index(){
$this->arr['list_js'] = array(
'sweetalert2.all.min',
'parsley/parsley.min',
'parsley/pt-br',
'jquery.mask.min',
'mask_init',
'confirm_atendimento',
'manager_atendimento'
);

$this->arr['grupos'] = (new Orm('grupos'))->select('*')->get();
$this->arr['empresas'] = (new Orm('unidade'))->select('*')->get();
$this->arr['motoristas'] = (new Orm('veiculos'))->select('*')->get();

$s = new Search();

$filter = array('voucher'=>'','grupo'=>'','empresa'=>'','motorista'=>'','data_ini'=>'', 'data_fim'=>'');

if(isset($_POST['empresa']) || isset($_POST['num_voucher']) || isset($_POST['grupo']) || isset($_POST['motorista'])){

$num_voucher = $this->limpaCampo($_POST['num_voucher']);
$grupo = $this->limpaCampo($_POST['grupo']);
$empresa = $this->limpaCampo($_POST['empresa']);
$motorista = $this->limpaCampo($_POST['motorista']);
$data_ini = $this->limpaCampo($_POST['data_ini']);
$data_fim = $this->limpaCampo($_POST['data_fim']);

if($num_voucher != null){  $filter['voucher'] = $num_voucher;}
if($grupo != null){  $filter['grupo'] = $grupo;}
if($empresa != null){  $filter['empresa'] = $empresa;}
if($motorista != null){  $filter['motorista'] = $motorista;}
if($data_ini != null){  $filter['data_ini'] = $this->converterData($data_ini,'/');}
if($data_fim != null){  $filter['data_fim'] = $this->converterData($data_fim,'/');}

$this->arr['dados'] = $s->getAllSearch('atendimentos', $filter, null);
$a = new Atendimento();
$this->arr['list'] = $a->atendimentos($this->arr['dados']);


echo $this->debug($this->arr['list']);
}

$this->loadTemplate('relatorios/atendimentos', $this->arr);
}//index



public function exportar(){

if(isset($_POST['empresa']) || isset($_POST['grupo']) || isset($_POST['motorista'])){
$grupo = $this->limpaCampo($_POST['grupo']);
$empresa = $this->limpaCampo($_POST['empresa']);
$motorista = $this->limpaCampo($_POST['motorista']);
$data_ini = $this->limpaCampo($_POST['data_ini']);
$data_fim = $this->limpaCampo($_POST['data_fim']);

if($data_ini != null){  $filter['data_ini'] = $this->converterData($data_ini,'/');}
if($data_fim != null){  $filter['data_fim'] = $this->converterData($data_fim,'/');}

$a = new Atendimento();
$this->arr['list'] = $a->buscaRelatorio2(null, $dados = array('id_grupo'=>$grupo,'id_empresa'=>$empresa,'veiculo'=>$motorista, 'data_ini'=>$data_ini, 'data_fim'=>$data_fim));

// Aqui começa a exportação
$formato = 'Xlsx';//Xlsx - Xls - Csv
$file = new Spreadsheet();
$active_sheet = $file->getActiveSheet();

$active_sheet->setCellValue('a1', 'Voucher');
$active_sheet->setCellValue('b1', 'Empresa');
$active_sheet->setCellValue('c1', 'Solicitante');
$active_sheet->setCellValue('d1', 'Passageiro');
$active_sheet->setCellValue('e1', 'Motorista');
$active_sheet->setCellValue('f1', 'Veiculo');
$active_sheet->setCellValue('g1', 'Placa');
$active_sheet->setCellValue('h1', 'Centrocusto');
$active_sheet->setCellValue('i1', 'Departamento');
$active_sheet->setCellValue('j1', 'Motivo');
$active_sheet->setCellValue('k1', 'Solicitação');
$active_sheet->setCellValue('l1', 'Embarque');
$active_sheet->setCellValue('m1', 'Desembarque');
$active_sheet->setCellValue('n1', 'Canal');
$active_sheet->setCellValue('o1', 'Cobrança');
$active_sheet->setCellValue('p1', 'Viagem');
$active_sheet->setCellValue('q1', 'Origem');
$active_sheet->setCellValue('r1', 'Destino');
$active_sheet->setCellValue('s1', 'Nota');
$active_sheet->setCellValue('t1', 'Km');
$active_sheet->setCellValue('u1', 'Hora parada');
$active_sheet->setCellValue('v1', 'Pedagio');
$active_sheet->setCellValue('w1', 'Estacionamento');
$active_sheet->setCellValue('x1', 'Justificativa');
$active_sheet->setCellValue('y1', 'Valor total');

$count = 2;
foreach($this->arr['list'] as $row){

if($row->tipo_valor == 'diaria'){
$viagem = 'Normal';
}else if($row->tipo_valor == 'km'){
$viagem = $row->tipo_km;
}else if($row->tipo_valor == 'taxi'){
$viagem = ($row->bandeira1 != null) ? 'Bandeira1': 'Bandeira2';
}else{
  $viagem = 'Normal';
}

switch($row->tipo_valor){
case 'diaria':
$hora_parada = ($row->hora_parada_km != '' OR $row->hora_parada_km != null) ? $row->hora_parada_km: 0;
break;

case 'km':
$hora_parada = ($row->hora_parada_km != '') ? $row->hora_parada_km: 0;
break;

case 'taxi':
$hora_parada = ($row->hora_espera != '') ? $row->hora_espera: 0;
break;

case 'fechado':
$hora_parada = 0;
break;
}



$active_sheet->setCellValue('a'.$count, $row->id_atendimento);
$active_sheet->setCellValue('b'.$count, $row->empresa);
$active_sheet->setCellValue('c'.$count, $this->user->getName() );
$active_sheet->setCellValue('d'.$count, $row->solicitante);
$active_sheet->setCellValue('e'.$count, $row->motorista->nome);
$active_sheet->setCellValue('f'.$count, $row->motorista->modelo);
$active_sheet->setCellValue('g'.$count, $row->motorista->placa);
$active_sheet->setCellValue('h'.$count, $row->centrocusto);
$active_sheet->setCellValue('i'.$count, $row->departamento);
$active_sheet->setCellValue('j'.$count, $row->motivo);
$active_sheet->setCellValue('k'.$count, $row->data_embarque." ".$row->hora_atendimento);
$active_sheet->setCellValue('l'.$count, $row->data_embarque." ".$row->hora_embarque);
$active_sheet->setCellValue('m'.$count, $row->data_desembarque." ".$row->hora_desembarque);
$active_sheet->setCellValue('n'.$count, "Sistema");
$active_sheet->setCellValue('o'.$count, $row->tipo_valor);
$active_sheet->setCellValue('p'.$count, $viagem);
$active_sheet->setCellValue('q'.$count, $row->origem->nome);
$active_sheet->setCellValue('r'.$count, $row->destino->nome);
$active_sheet->setCellValue('s'.$count, $row->nota);
$active_sheet->setCellValue('t'.$count, $row->total_km);
$active_sheet->setCellValue('u'.$count, $hora_parada);
$active_sheet->setCellValue('v'.$count, $row->pedagio);
$active_sheet->setCellValue('w'.$count, $row->estacionamento);
$active_sheet->setCellValue('x'.$count, $row->dados_gerais);
$active_sheet->setCellValue('y'.$count, $row->valor_total);
$count = $count +1;
}

$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($file, $formato);

$file_name = time().'.'.strtolower(($formato));
$writer->save($file_name);
header("Content-Type: application/x-www-form-urlencoded");
header("Content-Transfer-Encoding:Binary");
header("Content-disposition:attachment;filename=\"".$file_name."\"");
readfile($file_name);
unlink($file_name);
exit;
}//verifica se teve post

}//exportar



public function clientes(){
$this->arr['list_js'] = array(
'plugins/jquery.table-shrinker',
'shrinker_init', 
'plugins/jquery.mask.min',
'mask_init',
'plugins/sweetalert2.all.min'
); 

if(isset($_GET['nome'])){
$nome = addslashes( $_GET['nome']);
$this->arr['lista'] = (new Orm('clientes'))->select('*')->search(['nome', $nome])->get();	
}



$this->loadTemplate('relatorios/clientes', $this->arr);
}//clientes

public function produtos(){
$this->arr['list_js'] = array(
'plugins/jquery.table-shrinker',
'shrinker_init', 
'plugins/jquery.mask.min',
'mask_init',
'plugins/sweetalert2.all.min'
); 

if(isset($_GET['nome'])){
$nome     = addslashes( $_GET['nome']);
$data_ini = addslashes(urldecode($_GET['data_ini']));
$data_fim = addslashes(urldecode($_GET['data_fim']));

if($data_ini != ''){$data_ini = $this->converterData($data_ini,'/');}
if($data_fim != ''){$data_fim = $this->converterData($data_fim,'/');}


$this->arr['lista'] = (new Orm('cardapio'))->select('*')
->search(['nome_produto', $nome])
->e(['data_cad','>=', "\"".$data_ini."\""],['data_cad', '<=', "\"".$data_fim."\""])->paginate('10')
->get();	
}

$this->loadTemplate('relatorios/produtos', $this->arr);
}//produtos


public function pedidos(){
$this->arr['list_js'] = array(
'plugins/jquery.table-shrinker',
'shrinker_init', 
'plugins/jquery.mask.min',
'mask_init',
'plugins/sweetalert2.all.min'
); 

if(isset($_GET['codigo']) OR isset($_GET['metodo']) OR isset($_GET['status'])){
$id_pedido = addslashes( $_GET['codigo']);
$metodo    = addslashes($_GET['metodo']);
$status    = addslashes($_GET['status']);


$this->arr['lista'] = (new Orm('pedido'))->select('*')
->search(['id_pedido', $id_pedido])
->orSearch(['metodo_pagamento',$metodo],['status', $status])
->get();	
}

$this->loadTemplate('relatorios/pedidos', $this->arr);
}//clientes


}