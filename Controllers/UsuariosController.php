<?php
use Core\Controller;
use Models\Users;
use Models\Permissao;
use Models\Search;
use Models\Helpers;
use Models\Orm;

class UsuariosController extends Controller {

	private $user;
  private $arr;

  public function __construct() {              
    $this->user = new Users();

        if($this->user->isLogged() == false){                  
         header("Location: ".BASE_URL."login");        
        exit;         
  }  

        $this->arr = array(
         'user'=>$this->user,
         'menuActive'=>'home',
           'bread'=>'Usuários' 
        );     

      
    }

public function index() {
$this->arr['list_js'] = array(
'plugins/jquery.mask.min',
'mask_init',
'plugins/sweetalert2.all.min'
);  

$usuarios = new Orm('usuario');

//Filtrando pesquisas
$dados['filter'] = array('nome'=>'');
if(isset($_GET['nome']) && !empty($_GET['nome'])){
$dados['filter']['nome'] = $_GET['nome'];

$s = new Search();
$this->arr['listaUsers'] = $s->getAllSearch('usuario', $dados['filter']);
}else{
$this->arr['listaUsers'] = $usuarios->select('*')->paginate('10')->get();
$this->arr['paginacao'] = $usuarios->render('usuarios'); 
}

$this->arr['msg'] = $this->flashMessage($_SESSION['msg'] ?? null);

$this->loadTemplate('usuarios/usuarios', $this->arr);
}//function index   



public function add(){
$this->arr['list_js'] = array(
'plugins/parsley/parsley.min',
'plugins/parsley/pt-br',
'plugins/jquery.mask.min',
'mask_init',
'plugins/sweetalert2.all.min'
);    


$o = new Orm('usuario');
$cid = new Orm('cidade');
$est = new Orm('estado');
$this->arrayInfo['estados'] = $est->select('*')->get();
$this->arrayInfo['cidades'] = $cid->select('*')->get();


$this->loadTemplate('usuarios/usuarios_add', $this->arr);
}//add

public function add_action(){
$c = new Orm('usuario');
$h = new Helpers();

if(isset($_POST['email']) && !empty($_POST['nome'])){ 

$status = $this->limpaCampo($_POST['status']);
$tipo_usuario = $this->limpaCampo($_POST['tipo_usuario']);
$nome = $this->limpaCampo($_POST['nome']);
$email = $this->limpaCampo($_POST['email']); 
$telefone1 = $this->limpaCampo($_POST['telefone1']);   
$telefone2 = $this->limpaCampo($_POST['telefone2']);
$password = $this->limpaCampo($_POST['password']); 
$senha = password_hash($password, PASSWORD_DEFAULT);
$cargo = $this->limpaCampo($_POST['cargo']);
$setor = $this->limpaCampo($_POST['setor']);     
$admin = (isset($_POST['admin'])) ? '1' : '0'; 
$observacoes = $this->limpaCampo($_POST['observacoes']); 

$insere = $c->set([
'data_cad'=>date('Y-m-d H:i:s'),
'status'=>$status,
'tipo_usuario'=>$tipo_usuario,  
'nome'=>$nome,
'email'=>$email,
'telefone1'=>$telefone1,
'telefone2'=>$telefone2,
'senha'=>$senha,
'cargo'=>$cargo,
'setor'=>$setor,
'admin'=>$admin,
'observacoes'=>$observacoes
])->save();

$_SESSION['msg'] = "Usuário cadastrado com sucesso!";
 header("Location: ".BASE_URL."usuarios");
  exit;
}else{
   $_SESSION['formError'] = array('name');
   
   header("Location: ".BASE_URL."usuarios/add");
   exit; 
}

}//add action


public function edit($id){
$this->arrayInfo['list_js'] = array(
'parsley/parsley.min',
'parsley/pt-br',
'jquery.mask.min',
'mask_init',
'sweetalert2.all.min'
);  

$cid = new Orm('cidade');
$est = new Orm('estado');
$this->arrayInfo['estados'] = $est->select('*')->get();
$this->arrayInfo['cidades'] = $cid->select('*')->get(); 
$this->arrayInfo['bread'] = "Usuários Editar";      
$this->arrayInfo['errorItems'] = array();

// $this->permissao = new Permissao();        
// $this->permissao->temPermissao($usuario, 'super-admin', 'listar'); 

$o = new Orm('usuario');
$this->arrayInfo['info_usuario'] = $o->select('*')->where(['id', $id])->first()->get();
$this->arrayInfo['id_user'] = $id;

if(isset($_SESSION['formError']) && count($_SESSION['formError']) >0){
    $this->arrayInfo['errorItems'] = $_SESSION['formError'];
    unset($_SESSION['formError']);
}

$this->loadTemplate('usuarios/usuarios_edit', $this->arrayInfo);
}//edit


public function edit_action($id){
$h = new Helpers();
$c = new Orm('usuario');

if(isset($_POST['email']) && !empty($_POST['nome'])){ 

$status = $this->limpaCampo($_POST['status']);
$tipo_usuario = $this->limpaCampo($_POST['tipo_usuario']);
$nome = $this->limpaCampo($_POST['nome']);
$email = $this->limpaCampo($_POST['email']); 
$telefone1 = $this->limpaCampo($_POST['telefone1']);   
$telefone2 = $this->limpaCampo($_POST['telefone2']);
$password = $this->limpaCampo($_POST['password']); 
$senha = password_hash($password, PASSWORD_DEFAULT);
$cargo = $this->limpaCampo($_POST['cargo']);
$setor = $this->limpaCampo($_POST['setor']);     
$admin = (isset($_POST['admin'])) ? '1' : '0'; 
$observacoes = $this->limpaCampo($_POST['observacoes']); 

$insere = $c->set([
'status'=>$status,
'tipo_usuario'=>$tipo_usuario,  
'nome'=>$nome,
'email'=>$email,
'telefone1'=>$telefone1,
'telefone2'=>$telefone2,
'senha'=>$senha,
'cargo'=>$cargo,
'setor'=>$setor,
'admin'=>$admin,
'observacoes'=>$observacoes
])->where(['id', $id])->update();
$_SESSION['msg'] = 'Usuário editado com sucesso!';
header("Location: ".BASE_URL."usuarios");
exit;
}

}//edit action


public function reset(){
$this->arrayInfo['list_js'] = array(
'parsley/parsley.min',
'parsley/pt-br',
'jquery.mask.min',
'mask_init',
'sweetalert2.all.min'
);  

$c = new Orm('usuario');
$usuario = $this->user->getId();
$user = $c->select('*')->where(['id', $usuario])->first()->get();

if(isset($_POST['old_senha']) &&  !empty($_POST['senha']) && !empty($_POST['csenha'])){
 $senha = addslashes($_POST['old_senha']);
 $newSenha = addslashes($_POST['senha']); 
 $newpass = password_hash($newSenha, PASSWORD_DEFAULT);


if(password_verify($senha, $user->senha)){

 $form = new \stdClass();
 $form->senha = $newpass;

 $c->set(['senha'=>$newpass])->where(['id', $usuario])->update(); 
 $this->arrayInfo['msg'] = "Senha atualizada com sucesso!"; 
}else{
  $this->arrayInfo['msg'] = "senhas não conferem"; 
}

}

$this->loadTemplate('usuarios/reset', $this->arrayInfo);
}//reset

public function logout(){
session_destroy();
unset($_SESSION['token']);

header("Location: ".BASE_URL);
exit;
}//logout


public function del($id){  
$c = new Orm('usuario');
$utc = new Orm('usuario_tabela_acao_cliente');

$utc->del(['id_usuario', $id]); 
$c->del(['id', $id]);        

$_SESSION['msg'] = "Removido com sucesso!";
header("Location: ".BASE_URL."usuarios");
exit;
}//del

}