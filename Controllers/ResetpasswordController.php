<?php
use Core\Controller;
use Models\Users;
use Models\Permissao;
use Models\Orm;

class ResetpasswordController extends Controller {

	private $user;
  private $arr;

   public function __construct() {              
        $this->user = new Users();

        if($this->user->isLogged() == false){                  
         header("Location: ".BASE_URL."login");        
         exit;         
        }

        $this->arr = array(
         'user'=>$this->user,
         'menuActive'=>'home',
         'bread'=>'Reset',      
        );          
}

public function index() {   
$this->arr['list_js'] = array(
'plugins/jquery.table-shrinker',
'plugins/parsley/parsley.min',
'plugins/parsley/pt-br',
'shrinker_init', 
'plugins/jquery.mask.min',
'mask_init',
'plugins/sweetalert2.all.min',
);    

$usuario = $this->user->getId();
$old = (new Orm('usuario'))->select(['senha'])->where(['id', $usuario])->first()->get();

$this->arr['msg'] = $this->flashMessage($_SESSION['msg'] ?? null);


if(isset($_POST['senha']) && !empty($_POST['senha']) && !empty($_POST['senha'])){
 $antiga = $_POST['old_senha'];
 $senha  = $_POST['senha'];
 $csenha = $_POST['csenha'];
 $newSenha = password_hash($senha, PASSWORD_DEFAULT); 

if(password_verify($antiga, $old->senha)){

  if($senha == $csenha){
    $up = (new Orm('usuario'))->set(['senha'=>$newSenha])->where(['id', $usuario])->update();
    
    if($up == 1){
    $_SESSION['msg'] = 'Senha atualizada com sucesso!';
    $this->redirect(BASE_URL."resetpassword");  
    }
    
  }else{
    $_SESSION['msg'] = 'Senhas não conferem!';
    $this->redirect(BASE_URL."resetpassword");
  }

}else{
  $_SESSION['msg'] = 'Senha anterior não confere com a senha cadastrada!';
  $this->redirect(BASE_URL."resetpassword");
}
}



$this->loadTemplate('usuarios/reset', $this->arr);
}//function index   


}