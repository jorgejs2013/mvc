<?php
use Core\Controller;
use Models\Users;
use Models\Permissao;
use Models\Crud;
use Models\Helpers;
use Models\Orm;

class DepartamentoController extends Controller {

	private $user;
  private $arrayInfo;

   public function __construct() {              
        $this->user = new Users();

        if($this->user->isLogged() == false){                  
         header("Location: ".BASE_URL."login");        
         exit;         
        }  

        $usuario = $this->user->getid();
        $alias = 'super-admin';
        $this->permissao = new Permissao();                 
        $permissoes = $this->permissao->getPermissoes($usuario);

        $this->arrayInfo = array(
         'user'=>$this->user,
         'menuActive'=>'Departamentos',
         'bread'=>'Departamentos',
         'permissoes'=>$permissoes 
        );          
}

public function index() {   
$this->arrayInfo['list_js'] = array(
'sweetalert2.all.min',
'select_unidade',
'chosen.jquery.min'
);    

$this->arrayInfo['grupos'] = (new Orm('grupos'))->select('*')->get();

if(isset($_POST['nome']) && !empty($_POST['nome']) || isset($_POST['grupo'])){

  $nome = $this->limpaCampo($_POST['nome']);
  $grupo = $this->limpaCampo($_POST['grupo']);
  $unidade = $this->limpaCampo($_POST['unidade']);

  if($grupo != null){
  $this->arrayInfo['lista'] = (new Orm('departamentos'))->select('*')->where(['id_grupo', $grupo])
  ->e(['unidade', $unidade])->get();  
  }else{ 
    $this->arrayInfo['lista'] = (new Orm('departamentos'))->select('*')->search(['nome', $nome])->get();
  }  

}else{
$d = new Orm('departamentos'); 
$this->arrayInfo['lista'] = $d->select('*')->get();
}

if(isset($_SESSION['msg']) && !empty($_SESSION['msg'])){
  $this->arrayInfo['msg'] = $_SESSION['msg'];
  unset($_SESSION['msg']);
}

if(isset($_GET['nome']) && !empty($_GET['nome'])){
  $nome = $this->limpaCampo($_GET['nome']);
  $this->arrayInfo['lista'] = (new Orm('departamento'))->select('*')->search(['nome', $nome])->get();
}


$this->loadTemplate('departamentos/listar', $this->arrayInfo);
}//function index   

public function add(){
$this->arrayInfo['list_js'] = array(
'parsley/parsley.min',
'parsley/pt-br',
'jquery.mask.min',
'mask_init',
'select_unidade'
);    

$this->arrayInfo['grupos'] = (new Orm('grupos'))->select('*')->get();

if(isset($_SESSION['formError']) && count($_SESSION['formError']) >0){
$array['errorItems'] = $_SESSION['formError'];
unset($_SESSION['formError']);
}

$this->loadTemplate('departamentos/adicionar', $this->arrayInfo);
}//add

public function add_action(){
$d = new Orm('departamentos');

if(isset($_POST['nome']) && !empty($_POST['nome'])){
$grupo = $this->limpaCampo($_POST['grupo']);  
$unidade = $this->limpaCampo($_POST['unidade']);
$nome = $this->limpaCampo($_POST['nome']);
$responsavel           = $this->limpaCampo($_POST['responsavel']);
$email          = $this->limpaCampo($_POST['email']);


//gravando os dados
$insere = $d->set([
'id_grupo'=>$grupo,
'unidade'=>$unidade,
'nome'=>$nome,
'responsavel'=>$responsavel,
'email'=>$email
])->save();

$_SESSION['msg'] = "Cadastro realizado com sucesso!";
header("Location: ".BASE_URL."departamento");
exit;

}else{
   $_SESSION['formError'] = array('nome','ativo');   
   header("Location: ".BASE_URL."departamento/add");
   exit; 
}

}//add action


public function edit($id){
$this->arrayInfo['list_js'] = array(
'parsley/parsley.min',
'parsley/pt-br',
'jquery.mask.min',
'mask_init',
'select_unidade'
);     

$this->arrayInfo['grupos'] = (new Orm('grupos'))->select('*')->get();
$this->arrayInfo['id_departamento'] = $id;
$this->arrayInfo['info'] = (new Orm('departamentos'))->select('*')->where(['id_departamento', $id])->first()->get();

$this->arrayInfo['unidade'] = (new Orm('unidade'))->select('*')->where(['id_grupo', $this->arrayInfo['info']->id_grupo])->first()->get();

if(isset($_SESSION['formError']) && count($_SESSION['formError']) >0){
$array['errorItems'] = $_SESSION['formError'];
unset($_SESSION['formError']);
}

$this->loadTemplate('departamentos/editar', $this->arrayInfo);
}//edit

public function edit_action($id){
$d = new Orm('departamentos');

if(isset($_POST['nome']) && !empty($_POST['nome'])){
$grupo = $this->limpaCampo($_POST['grupo']);  
$unidade = $this->limpaCampo($_POST['unidade']);
$nome = $this->limpaCampo($_POST['nome']);
$responsavel           = $this->limpaCampo($_POST['responsavel']);
$email          = $this->limpaCampo($_POST['email']);


//gravando os dados
$insere = $d->set([
'id_grupo'=>$grupo,
'unidade'=>$unidade,
'nome'=>$nome,
'responsavel'=>$responsavel,
'email'=>$email
])->where(['id_departamento', $id])->update();

$_SESSION['msg'] = "Cadastro atualizado com sucesso!";
header("Location: ".BASE_URL."departamento");
exit;

}else{
   $_SESSION['formError'] = array('nome','ativo');   
   header("Location: ".BASE_URL."departamento/add");
   exit; 
}
}//edit action

public function del($id){
$c = new Orm('departamentos');
$c->del(['id_departamento', $id]);

$_SESSION['msg'] = "departamento removido com sucesso!";
header("Location: ".BASE_URL."departamentos");
exit; 
}//del

}