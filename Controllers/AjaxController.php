<?php
use Core\Controller;
use Models\Users;
use Models\Email;
use Models\Orm;
use Models\Permissao;

class AjaxController extends Controller{

private $user;
private $arrayInfo;

public function __construct() {              
$this->user = new Users();

if($this->user->isLogged() == false){                  
 header("Location: ".BASE_URL."login");        
 exit;         
}  


$this->arrayInfo = array(
  'user'=>$this->user 
);  

}//construtor


public function consultarPassageiro(){
$telefone = filter_input(INPUT_POST, 'telefone');

$passageiro = (new Orm('passageiros'))->select('*')->search(['celular', $telefone])->first()->get();


$passageiro->empresa = (new Orm('unidade'))->select('*')->where(['id_unidade', $passageiro->id_unidade])->first()->get();
$passageiro->grupo   = (new Orm('grupos'))->select('*')->where(['id_grupo', $passageiro->id_grupo])->first()->get();
$passageiro->centro  = (new Orm('centrocusto'))->select('*')->where(['id_centrocusto', $passageiro->centro_custo])->first()->get(); 


echo json_encode($passageiro);
exit;

}//consultarPassageiro

public function changeUF(){
$estado = filter_input(INPUT_POST, 'estado'); 
$dados = (new Orm('cidade'))->select('*')->where(['id_estado', $estado])->get();

echo json_encode($dados);
exit;
}

public function changeGrupo(){
$grupo = filter_input(INPUT_POST, 'grupo'); 

$dados = (new Orm('unidade'))->select('*')->where(['id_grupo', $grupo])->get();
echo json_encode($dados);
exit;
}//changeGrupo

public function consultarVeiculo(){
$id_veiculo = filter_input(INPUT_POST, "veiculo");
$veiculo = (new Orm('motorista'))->select('*')->where(['id_motorista', $id_veiculo])->first()->get();

echo json_encode($veiculo);
exit;
}//consultarVeiculo

public function consultaLocal(){
$id_local = filter_input(INPUT_POST, 'local');
$local = (new Orm('locais'))->select('*')->where(['id_local', $id_local])->first()->get();

echo json_encode($local);
exit;
}//consultaLocal


public function consultaCliente(){
$c = new Crud();
$telefone = filter_input(INPUT_POST, 'telefone');
$data     = $c->listaComJoin('*', 'contato', array(['cidade'], ['estado']),
array(['contato.id_cidade', '=', 'cidade.id_cidade'], ['contato.id_estado', '=', 'estado.id_estado']),
"celular='{$telefone}' AND eh_cliente='S'", false);

echo json_encode($data);
exit;
}//consultaCliente

public function atualizaSituacao(){
$c = new Crud();    
$situacao = filter_input(INPUT_POST, 'situacao');
$id_delivery = filter_input(INPUT_POST, 'id_delivery');

$form = new \stdClass();
$form->status_delivery = $situacao;

$atualiza = $c->atualizar('delivery', $form, "id_delivery={$id_delivery}");

if($atualiza >0){
    echo json_encode('1');
    exit;
}else{
    echo json_encode('2');
    exit;
}

}//


public function consultaClientePorNome(){
$c = new Crud();
$cliente = filter_input(INPUT_POST, 'cliente');
$data     = $c->listaComJoin('*', 'contato', array(['cidade'], ['estado']),
array(['contato.id_cidade', '=', 'cidade.id_cidade'], ['contato.id_estado', '=', 'estado.id_estado']),
"id_contato='{$cliente}' AND eh_cliente='S'", false);

echo json_encode($data);
exit;
}//consultaCliente

public function ncms(){
$c = new Crud();
$data = array();

if(isset($_GET['pesq'])){
$q = $_GET['pesq'];

$result =  $c->search('ncms', 'descricao', $q, true);
if(COUNT($result) > 0){
foreach($result as $row){
    $data[] = array('id'=>$row->ncm, 'text'=>$row->ncm.' - '.$row->descricao);
}
}else{
    $data[] = array('id'=>0, 'text'=>'Nenhum NCM localizado');
}
}

echo json_encode($data);
exit;
}//ncms

public function clientes(){
$c = new Crud();
$data = array();

if(isset($_GET['pesq'])){
$q = $_GET['pesq'];

$result =  $c->search('contato', 'nome', $q, true);
if(COUNT($result) > 0){
foreach($result as $row){
    $data[] = array('id'=>$row->id_contato, 'text'=>$row->id_contato.' - '.$row->nome);
}
}else{
    $data[] = array('id'=>0, 'text'=>'Nenhum cliente localizado');
}
}

echo json_encode($data);
exit;
}//clientes


public function search_products(){
$c = new Crud();

if(isset($_GET['pesq']) && !empty($_GET['pesq'])){
$q = addslashes($_GET['pesq']);

$data = $c->search('produtos','produto', $q);

echo json_encode($data);
exit;
}
}//search products


public function updateAvatar(){
$data = $_POST['image'];
//data:image/png;base64,ipoajdfiajsdfopiajsdfpoiajfpoisajfpio
$image_array_1 = explode(';', $data);
$image_array_2 = explode(",", $image_array_1[1]);
$usuario = $_SESSION['user'];

$data = base64_decode($image_array_2[1]);

$imageName = time().'.png';

file_put_contents("medias/capas/".$imageName, $data);

$qr = new QueryBuilder('usuario');
$atualizado = $qr->set(['imagem'=> $imageName])
                 ->where(['id',$usuario])
                 ->update();

}//updateAvatar



public function uploads(){
	if(!empty($_FILES['file']['tmp_name'])){
		$types_allowed = array('image/jpeg', 'image/png');

		if(in_array($_FILES['file']['type'], $types_allowed)){

            $newname = md5(time().rand(0,999).rand(0,999)).'.jpg';
			move_uploaded_file($_FILES['file']['tmp_name'], 'medias/'.$newname);

            $array = array(
             'location'=> BASE_URL."medias/".$newname
            );
            echo json_encode($array);
		    exit;
		}
	}

}//uploads


public function upload(){
$folder_name = 'medias/';

//quando faz o upload
if(!empty($_FILES)){

  $temp_file = $_FILES['file']['tmp_name'];
  $location = $_FILES['file']['name'];
  $location = str_replace(' ', '-', $location); 
 
$imagens = new Orm('banners');
$inserir = $imagens->set(['imagem'=>$location, 'link'=>$link, 'status_btn'=>$status])->save();
  
move_uploaded_file($temp_file, $folder_name.$location);
}
//quando exclui a imagem
if(isset($_POST['name'])){
  $imagem = $_POST['imagem'];
  
  $filename = $folder_name.$_POST['name'];
  unlink($filename);

  $qr = new Orm('banners');
    $deletar = $qr->del(['id_banner', $imagem]);
}

//pegando as imagens da pasta
//pegando as imagens da pasta
$result = array();
$files = scandir('medias/');
$saida = '<div>';

$imagens = new Orm('banners');
$dados['imagens'] = $imagens->select('*')->get();

foreach ($dados['imagens'] as $imagem){
  $saida .= '<div class="imgs_drop"><img src="'.BASE_URL.'medias/'.$imagem['imagem'].'" width="150" height="100" data-dz-remove /><button type="button" class="remove_image" data-id="'.$imagem['id_banner'].'" id="'.$imagem['imagem'].'">Remover</button></div>';
}

$saida .= '</div>';
echo $saida;
}//upload


public function rastreio(){
$id_pedido = filter_input(INPUT_POST, 'pedido');
$rastreio = filter_input(INPUT_POST, 'rastreio');

$up = (new Orm('pedido'))->set(['rastreio'=>$rastreio, 'status'=>4])->where(['id_pedido', $id_pedido])->update();

echo json_encode('atualizado');
exit;
}//rastrei


public function statusPendente(){
$atendimento = filter_input(INPUT_POST, 'atendimento');
$data = filter_input(INPUT_POST, 'data');
$hora = filter_input(INPUT_POST, 'hora');

$up = (new Orm('atendimentos'))
->set(['status'=>'2','data_atendimento'=>$data, 'hora_atendimento'=>$hora])
->where(['id_atendimento', $atendimento])
->update();

if($up){
echo json_encode('atualizado');
exit;  
}else{
  echo json_encode('error');
  exit;
}


}//statusPendente


public function getCentroDepartamento(){
$grupo = filter_input(INPUT_POST, 'grupo');

$centros = (new Orm('centrocusto'))->select('*')->where(['id_grupo', $grupo])->get();
$departamentos = (new Orm('departamentos'))->select('*')->where(['id_grupo', $grupo])->get();

$retorno = array(
'centro'=>$centros,
'departamento'=>$departamentos
);

echo json_encode($retorno);
exit;
}//$status = filter_input(INPUT_POST, 'status');


public function atendimentos(){
$status = filter_input(INPUT_POST, 'status');


$atendimentos = (new Orm('atendimentos'))->select('*')->where(['status',$status])->get();
foreach($atendimentos as $indice => $at){

$atendimentos[$indice]->passageiro = (new Orm('passageiros'))->select('*')->where(['id_passageiro', $atendimentos[$indice]->id_passageiro])->first()->get(); 

$atendimentos[$indice]->empresa = (new Orm('unidade'))->select(['razao_social'])
->where(['id_unidade', $at->id_empresa])
->first()->get(); 

$atendimentos[$indice]->origem = (new Orm('locais'))->select('*')
->where(['id_local', $at->local_origem])
->first()->get();

$atendimentos[$indice]->destino = (new Orm('locais'))->select('*')
->where(['id_local', $at->local_destino])
->first()->get();

if($at->veiculo == null OR $at->veiculo == 0){
$atendimentos[$indice]->veiculo = (new Orm('avulsos'))->select('*')->where(['id_atendimento', $at->id_atendimento])->first()->get(); 
}else{
$atendimentos[$indice]->veiculo = (new Orm('veiculos'))->select('*')
->where(['id_veiculo', $at->veiculo])
->first()->get();
}

}


$saida  = '';
foreach($atendimentos as $atendimento):

$funcao = "confirma_atendimento(this)";
  
$saida .= '<tr onclick="'.$funcao.'" data-date="'. $atendimento->data_atendimento.'" data-hora="'. $atendimento->hora_atendimento.'" data-atendimento="'.$atendimento->id_atendimento.'" data-status="'.$status.'" style="cursor: pointer;">';
$veiculo_nome = (isset($atendimento->veiculo->nome) && $atendimento->veiculo->nome != null) ? $atendimento->veiculo->nome : '';
$veiculo_contato = (isset($atendimento->veiculo->telefone1)) ? $atendimento->veiculo->telefone1: '' ;
$veiculo_modelo  = (isset($atendimento->veiculo->modelo)) ? $atendimento->veiculo->modelo : '' ;
$veiculo_placa   = (isset($atendimento->veiculo->placa)) ? $atendimento->veiculo->placa : '';
$nome_destino    =  $atendimento->destino->nome ?? '';
$razao_social = $atendimento->empresa->razao_social ?? '';
$nome_passageiro = $atendimento->passageiro->nome ?? '';
$celular_passageiro = $atendimento->passageiro->celular ?? '';
$origem_nome = $atendimento->origem->nome ?? '';
$saida .= '<td>'. str_pad($atendimento->id_atendimento, 5, "0", STR_PAD_LEFT).'</td> '; 
$saida .= '<td>'. $atendimento->id_atendimento.'</td>';
$saida .= '<td>'. $atendimento->solicitante.'</td>';
$saida .= '<td>'. $razao_social.'</td>';
$saida .= '<td>'. $nome_passageiro.'</td>';
$saida .= '<td>'. $celular_passageiro.'</td>';
$saida .= '<td>'. $origem_nome.'</td>';
$saida .= '<td>'. $nome_destino.'</td>';
$saida .= '<td>'.$veiculo_nome.'</td>';
$saida .= '<td>'. $veiculo_contato.'</td>';
$saida .= '<td>'.$veiculo_modelo .'</td>';
$saida .= '<td>'.$veiculo_placa.'</td>';
$saida .= '<td>'. $atendimento->status.'</td>';
$saida .= '<td>'. $atendimento->status.'</td>';
$saida .= '</tr>';
endforeach;

echo $saida;
exit;
}//atendimentos

}//fim da class ajaxController