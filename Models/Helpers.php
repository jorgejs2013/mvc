<?php
namespace Models;
use Core\Model;
use PDO;

class Helpers extends Model{

private $Data;
private $Format;

public function Name($Name){
$this->Format = array();
$this->Format['a'] = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿRr"!@#$%&*()_-+={[}]/?;:.,\\\'<>°ºª';	
$this->Format['b'] = 'aaaaaaaceeeeiiiidnoooooouuuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr                                 ';
$this->Data = strtr(utf8_decode($Name), utf8_decode($this->Format['a']), utf8_decode($this->Format['b']));	
$this->Data = strip_tags(trim($this->Data));

$this->Data = str_replace(' ','-', $this->Data);
$this->Data = str_replace(array('-----', '----', '---','--'),'-', $this->Data);

return strtolower(utf8_encode($this->Data));
}//fim function name


static function VoltarPagina(){
         
         echo '<script> function goBack() {
                    window.history.back();
                    } </script>';
         echo '<button onclick="goBack()" class="btn btn-default">'
         . '<i class="glyphicon glyphicon-circle-arrow-left" ></i> Voltar</button> ';
}//voltarPagina

public function Email($Email){
$this->Data = (string) $Email;
$this->Format = '/[a-z0-9_\.\-]+@[a-z0-9_\.\-]*[a-z0-9_\.\-]+\.[a-z]{2,4}$/';

if(preg_match(self::$Format, $this->Data)){
return true;
}else{
return false;
}
}//verifica email



public function loadJS($files, $page){
$url = explode("/",$_GET['url'][0]);
if($page == $url){
    foreach ($files as $key => $value) {
        echo '<script src="'.INCLUDE_PATH_PAINEL.'js/'.$value.'"></script>';
    }
}   
}//loadJs


public static function limparUsuariosOnline(){
$date = date('Y-m-d H:i:s');
$sql = MySql::conectar()->exec("DELETE FROM `tb_admin.online` WHERE ultima_acao < '$date' - INTERVAL 1 MINUTE");
}//limpaUsuarioOnline


public static function alert($tipo, $mensagem){
    if($tipo == 'sucesso'){
        echo '<div class="box-alert sucesso"><i class="fa fa-check"></i> '.$mensagem.'</div>';
    }else if($tipo == 'erro'){
        echo '<div class="box-alert erro"><i class="fa fa-times"></i> '.$mensagem.'</div>';
    }
}//alert


public static function redirect($url){
    echo '<script>location.href="'.$url.'"</script>';
    die();
}//redirect


public static function imagemValida($imagem){
            if($imagem['type'] == 'image/jpeg' ||
                $imagem['type'] == 'imagem/jpg' ||
                $imagem['type'] == 'imagem/png'){

                $tamanho = intval($imagem['size']/1024);
                if($tamanho < 300)
                    return true;
                else
                    return false;
            }else{
                return false;
            }
}//imagemValida




public static function formatarMoeda($valor){
$valor = str_replace('.', '', $valor);
$valor = str_replace(',', '.', $valor);
return $valor;  
}//formatarMoeda

public function converterMoeda($valor){
 return number_format($valor, 2,',','.');   
}//converterMoeda


public function formatarData($data){
$data = explode('/', $data);
$newData = $data[2].'-'.$data[1].'-'.$data[0];
return $newData;
}//converte Data



function limitaTexto($string, $words = '100'){
$string = strip_tags($string);
$count = strlen($string);

if($count <= $words){
	return $string;
	}else{
		$strpos = strrpos(substr($string, 0, $words), ' ');
		return substr($string,0, $strpos).'...';
		}	
}


public function Words($String, $Limite, $Pointer = null){

$this->Data = strip_tags(trim($String));
$this->Format = (int) $Limite;

$ArrWords = explode(' ', $this->Data);
$NumWords = count($ArrWords);
$NewWords = implode(' ', array_slice($ArrWords, 0, $this->Format));

$Pointer = (empty($Pointer) ? '...' : ' '.$Pointer);
$Result = ($this->Format < $NumWords ? $NewWords.$Pointer : $this->Data);
return $Result;

}//fim Words


function upload($arquivo, $dir, $nome, $largura = null, $altura = null){

$tipo = $arquivo['type'];
$image = $arquivo['tmp_name'];

if($tipo == 'image/gif' ){
 $nomeImagem = $nome.md5(time()).'.gif';
 move_uploaded_file($image,  $dir.$nomeImagem);
 return $nomeImagem;   
} 

list($largura_original, $altura_original) = getimagesize($image);
$ratio = $largura_original / $altura_original;

if($largura / $altura > $ratio){
    $largura = $altura* $ratio;
}else{
    $altura = $largura / $ratio;
}

$imagem_final = imagecreatetruecolor($largura, $altura);
//verifica se a imagem é png ou jpg 
if($tipo == 'image/png'){
imagealphablending($imagem_final, false); //Desabilita a mesclagem
imagesavealpha($imagem_final, true);//canal alpha
$transparent = imagecolorallocatealpha($imagem_final, 255, 255, 255, 127);
imagefilledrectangle($imagem_final, 0, 0, $largura, $altura, $transparent);
$imagem_original = imagecreatefrompng($image);

}else{
$imagem_original = imagecreatefromjpeg($image); 
}

imagecopyresampled($imagem_final, $imagem_original, 0, 0, 0, 0, $largura, $altura, $largura_original, $altura_original);

if($tipo == 'image/jpeg' || $tipo == 'image/jpg'){
$nomeImagem = $nome.md5(time()).'.png';
imagepng($imagem_final, $dir.$nomeImagem);
}else{
$nomeImagem = $nome.md5(time()).'.jpg';
imagejpeg($imagem_final, $dir.$nomeImagem, 70); 
}

imagedestroy($imagem_final);
return $nomeImagem;
}//upload


function EXPORT_TABLES($host,$user,$pass,$name, $tables=false, $backup_name=false){ 
    set_time_limit(3000); 

    $mysqli = new mysqli($host,$user,$pass,$name); 
    $mysqli->select_db($name); 
    $mysqli->query("SET NAMES 'utf8'");
    $queryTables = $mysqli->query('SHOW TABLES');
   
    while($row = $queryTables->fetch_row()) { 
        $target_tables[] = $row[0]; 
    }  
   

    if($tables !== false) { 
      $target_tables = array_intersect( $target_tables, $tables); 
    } 
   

    $content = "SET SQL_MODE = \"NO_AUTO_VALUE_ON_ZERO\";\r\nSET time_zone = \"+00:00\";\r\n\r\n\r\n/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;\r\n/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;\r\n/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;\r\n/*!40101 SET NAMES utf8 */;\r\n--\r\n-- Database: `".$name."`\r\n--\r\n\r\n\r\n".PHP_EOL;


    foreach($target_tables as $table){
        if (empty($table)){ 
            continue; 
        } 

        $result = $mysqli->query('SELECT * FROM `'.$table.'`');     
        $fields_amount = $result->field_count;  
        $rows_num = $mysqli->affected_rows;     
        $res = $mysqli->query('SHOW CREATE TABLE '.$table); 
        $TableMLine = $res->fetch_row();


        $content .= "\n\n".$TableMLine[1].";\n\n";

        for ($i = 0, $st_counter = 0; $i < $fields_amount;   $i++, $st_counter=0) {
            while($row = $result->fetch_row())  { 
            //when started (and every after 100 command cycle):
                if ($st_counter%100 == 0 || $st_counter == 0 )  {
                    $content .= "\nINSERT INTO ".$table." VALUES";
                }
                    
                    $content .= "\n("; 

                     for($j=0; $j <$fields_amount; $j++){ 
                        $row[$j] = str_replace("\n","\\n", addslashes($row[$j]) ); 
                        if (isset($row[$j])){$content .= '"'.$row[$j].'"' ;}  
                        else{$content .= '""';} 

                         if ($j<($fields_amount-1)){$content.= ',';}   }       
                          $content .=")".PHP_EOL;

                //every after 100 command cycle [or at last line] ....p.s. but should be inserted 1 cycle eariler
                if ( (($st_counter+1)%100==0 && $st_counter!=0) || $st_counter+1==$rows_num) {$content .= ";";} 
                else {$content .= ",";} 
                $st_counter = $st_counter + 1;
            }
        } $content .="\n\n\n".PHP_EOL;
    }

    $content .= "\r\n\r\n/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;\r\n/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;\r\n/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;".PHP_EOL;
    
    $backup_name = $backup_name ? $backup_name : $name."___(".date('H-i-s')."_".date('d-m-Y').")__rand".rand(1,11111111).".sql";
    
    ob_get_clean(); 
    header('Content-Type: application/octet-stream');   
    header("Content-Transfer-Encoding: Binary"); 
    header("Content-disposition: attachment; filename=\"".$backup_name."\"");
    echo $content; 
    exit;
}   //see import.php too
//EXPORT_TABLES("localhost", "root", "", "prime");


/* Backup do DB ou de uma tabela */
function backup_tables($tables = '*')
{
global $config;

$link = new PDO("mysql:dbname=".$config['dbname'].";host=".$config['host'], $config['dbuser'], $config['dbpass']);
$link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    
    //Pega tudo das tabelas
    if($tables == '*')
    {
        $tables = array();
        $result = $link->query('SHOW TABLES');
        while($row = $result->fetch())
        {
            $tables[] = $row[0];
        }
    }
    else
    {
        $tables = is_array($tables) ? $tables : explode(',',$tables);
    }
    
     $return = '';  
    //cycle through
    foreach($tables as $table)
    {
        $result = $link->query('SELECT * FROM '.$table);        
        $num_fields = $result->columnCount();  
        
        $return.= 'DROP TABLE '.$table.';';         
        $sql = $link->query('SHOW CREATE TABLE '.$table);
        $row2 = $sql->fetch(PDO::FETCH_NUM);
        
        $return.= "\n\n".$row2[1].";\n\n";
                
        for ($i = 0; $i < $num_fields; $i++) 
        {
            while($row = $result->fetch(PDO::FETCH_NUM))
            {
               
                $return.= 'INSERT INTO '.$table.' VALUES(';  

                for($j=0; $j < $num_fields; $j++) 
                {
                    
                    $row[$j] =  str_replace("\n","\\n", addslashes($row[$j]) );
                    if (isset($row[$j])) { 
                        $return.= '"'.$row[$j].'"' ; 
                    } else { 
                        $return.= '""'; 
                    }
                    if ($j < ($num_fields-1)) { $return.= ','; }
                }
                $return.= ");\n";               
            }
        }
        $return.="\n\n\n";
    }
    
    //save file
    $handle = fopen('db-backup-'.time().'-'.(md5(implode(',',$tables))).'.sql','w+');
    fwrite($handle,$return);
    fclose($handle);
}

public function pagination($tabela, $cond, $maximos, $link, $pag, $width = NULL, $maxlinks = 4){
//leitura do banco de dados    
$sql = "SELECT * FROM $tabela";
if($cond != null){
    $sql .= " $cond";
}

$readPaginator = $this->db->query($sql);
$resultado = $readPaginator->fetchAll();
$total = count($resultado);

$saida = '';

if($total >= $maximos){
    $paginas = ceil($total / $maximos);  

 if($width){
   $saida .= '<div class="paginator" style="width:'.$width.'">'; 
 }else{
   $saida .= '<div class="paginator">'; 
 }

$saida .= '<a href="'.$link.'1">Primeira Página</a>&nbsp;&nbsp;&nbsp;';

for($i = $pag - $maxlinks;$i <= $pag -1; $i++){
    if($i >=1){
        $saida .= '<a href="'.$link.$i.'">'.$i.'</a>&nbsp;&nbsp;&nbsp;';
    }    
}

$saida .= '<span class="atv">'.$pag.'</span>&nbsp;&nbsp;&nbsp;';

for($i = $pag + 1;$i <= $pag + $maxlinks; $i++){
    if($i <= $paginas){
        $saida .= '<a href="'.$link.$i.'">'.$i.'</a>&nbsp;&nbsp;&nbsp;';
    }    
}

 $saida .= '<a href="'.$link.$paginas.'">Última Página</a>';
 $saida .= '</div>'; 

 return $saida;
}
}//pagination



public function diferencaEmHora($d1, $d2){
$diferenca = $d1->diff($d2, true);
return 24 * $diferenca->d + $diferenca->h;
}//diferença em hora

public function diferencaEmDias($d1, $d2){
$diferenca = $d1->diff($d2, true);
return $diferenca->d;
}//diferença em dias



public function somarDataMysql($data, $ano, $meses, $dias){
$data = explode("-", $data);
$resData2 = date('Y-m-d', mktime(0, 0, 0, $data[1] + $meses, $data[2] + $dias, $data[0] + $ano));
return $resData2;
}//somarDataMysql

}
