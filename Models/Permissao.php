<?php
namespace models;
use core\Model;
use PDO;

class Permissao extends model{

public function getPermissao($id_usuario, $id_tabela, $id_acao){
$sql = "SELECT * FROM usuario_tabela_acao_cliente WHERE 
id_usuario = :id_usuario AND 
id_tabela = :id_tabela AND 
id_acao = :id_acao";
$sql = $this->db->prepare($sql);
$sql->bindValue(":id_usuario", $id_usuario);
$sql->bindValue(":id_tabela", $id_tabela);
$sql->bindValue(":id_acao", $id_acao);
$sql->execute();

return $sql->fetch();
}//getPermissao

public function getPermissoes($id_usuario){
$array = array();

$sql = "SELECT * FROM usuario_tabela_acao_cliente WHERE id_usuario = :id_usuario";
$sql = $this->db->prepare($sql);
$sql->bindValue(":id_usuario", $id_usuario);
$sql->execute();
$result = $sql->fetchAll();


foreach($result as $key => $opc){
  $array[$key]['tabela'] = $this->nameTabela($opc->id_tabela);
  $array[$key]['acao'] = $this->nameAcao($opc->id_acao);  
}

return $array;
}//getPermissoes


public function nameTabela($id_tabela){
$array = array();	
$sql = "SELECT alias_tabela FROM usuario_tabela WHERE id_tabela = '$id_tabela'";
$sql = $this->db->query($sql);

if($sql->rowCount()>0){
 return $sql->fetch()->alias_tabela;		
}

return $array;
}//nameTabela

public function nameAcao($id_acao){
$sql = "SELECT alias_acao FROM usuario_acao WHERE id_acao = '$id_acao'";
$sql = $this->db->query($sql);
return $sql->fetch()->alias_acao;	
}//nameAcao


public function inserir($id_usuario, $id_tabela, $id_acao){
$sql = "INSERT INTO usuario_tabela_acao_cliente SET id_tabela = :id_tabela, id_acao = :id_acao, id_usuario = :id_usuario";
$sql = $this->db->prepare($sql);
$sql->bindValue(":id_tabela", $id_tabela);
$sql->bindValue(":id_acao", $id_acao);
$sql->bindValue(":id_usuario", $id_usuario);
$sql->execute();

return $this->db->lastInsertId();
}//inserir

public function getPorAlias($id_usuario, $alias_tabela, $alias_acao = null){
$sql = "SELECT * FROM usuario_tabela_acao_cliente tac, usuario_tabela t, usuario_acao a 
WHERE tac.id_tabela = t.id_tabela AND
tac.id_acao = a.id_acao AND
tac.id_usuario = '$id_usuario' AND
alias_tabela = '$alias_tabela' ";

if($alias_acao){
	 $sql .= " AND alias_acao = '$alias_acao'";
}

$sql = $this->db->query($sql);

return $sql->fetch();
}//getTabelaPorAlias

public function temPermissao($id_usuario, $alias_tabela, $alias_acao = null){
  $tem = $this->getPorAlias($id_usuario, $alias_tabela, $alias_acao);
  if(!$tem){
  	header("Location:".BASE_URL."permissao/semPermissao");
  	exit;
  }
}//temPermissao


public function excluir($id_usuario, $id_tabela, $id_acao){
$sql = "DELETE FROM usuario_tabela_acao_cliente WHERE
id_usuario = :id_usuario AND id_tabela = :id_tabela AND id_acao = :id_acao";

$sql = $this->db->prepare($sql);
$sql->bindValue(":id_usuario", $id_usuario);
$sql->bindValue(":id_tabela", $id_tabela);
$sql->bindValue(":id_acao", $id_acao);
$sql->execute();
}//excluir


public function listaAcoes($id_tabela){

$sql = "SELECT * FROM usuario_tabela_acao
INNER JOIN usuario_tabela
ON usuario_tabela_acao.id_tabela = usuario_tabela.id_tabela
INNER JOIN usuario_acao
ON usuario_tabela_acao.id_acao = usuario_acao.id_acao
WHERE usuario_tabela_acao.id_tabela = $id_tabela";
$sql = $this->db->query($sql);	

return $sql->fetchAll();
       
}//lista acoes

}