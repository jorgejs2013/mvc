<?php
namespace models;
use core\Model;
use PDO;

class Relatorio extends model{

	private $pdf;
    private $colunas;
    private $Titulo;

	public function __construct(){     
       //criando pdf object
        $this->pdf = new \Fpdf("P", "mm", "A4"); //mm - pt                    
	}//construtor

    public function setTitulo($titulo){
        $this->Titulo = $titulo;
    }//setTitulo

    public function getTitulo(){
        return $this->Titulo;
    }

	public function start(){
        $this->pdf->AliasNbPages();
        $this->pdf->AddPage();    

        $this->pdf->SetFont('Arial', 'B', 14);
        $this->pdf->SetTextColor(0,0,0);

        //Criando o titulo do relatorio
        //Cell(float w [, float h [, string txt [, mixed border [, int ln [, string align [, boolean fill [, mixed link]]]]]]])
        $texto = utf8_decode($this->Titulo);
        $this->pdf->cell(0,15,$texto,0,0,'C');
        $this->pdf->Ln(20); 

        //Chamando o cabecalho
        $this->header();       
    }//relatorio

    //site com tutorial do fpdf
    //fpdf.org/en/tutorial

    private function header(){
        /*  $this->pdf->SetFont('Arial','',10);
            $this->pdf->cell(10,20, utf8_decode($relatorioCriadoDia), 0,0,'L');
            $this->pdf->Ln();

            $this->pdf->cell(10,5, utf8_decode($relatorioCriadoPor),0,0,'L');
            $this->pdf->Ln(30);
        */
        $data = str_replace('/','_', date('d/m/Y'));
        $relatorioCriadoDia = 'Relatorio criado no dia: '.date('d/m/Y');
        $relatorioCriadoPor = "Relatorio Criado por Admin";
        //local - 
       // $this->pdf->Image("relatorio-ico.png", 20, 10, 50);
        //$this->pdf->SetFont("Arial", "B", 12);
        //
        
        $this->pdf->SetFont("Arial", "B", 12);
        //$this->pdf->Cell(0, 30, "Lista de Clientes",0, 0, "C");

        $this->pdf->SetFont("Arial", "B", 8);
        $this->pdf->Cell(0, 6, utf8_decode("Relatório impresso em: ").date('d-m-Y H:i:s'),0,0,"R");
        $this->pdf->Ln();//espaco
        $this->pdf->cell(0, 6, utf8_decode($relatorioCriadoPor),0,0,'R');
        //distancia do cabecalho para a tabela com os dados 
        $this->pdf->Ln(20);
    }//header

    private function footer(){
        $this->pdf->SetY(-35);
        $this->pdf->SetFont("Arial", "I", 8);
        $this->pdf->Cell(0, 10, "Visite nosso site - www.lojavirtual.com.br",0,0, "C", false, "https://lojavirtual.com.br");
        $this->pdf->Cell(0, 10, 'pagina: '.$this->pdf->PageNo()."/{nb}", 0, 0, "R", false);
        $this->pdf->Ln();
    }//footer

    public function ConteudoPrincipal($dados){ 
        //dados do cliente 
        $this->pdf->SetFont("Arial", "B", 14);    
        $this->pdf->cell(100, 6, 'Dados do cliente', 0,0,'L');



        // echo "<pre>";
        // print_r($dados);
        // exit;

        //Anexos
        $this->pdf->SetFont("Arial", "B", 14);    
        $this->pdf->cell(90, 6, utf8_decode('Comprovantes'), 0,0,'C');
        if(!empty($dados->anexos[0]->nome_arquivo)){

         $ext = pathinfo($dados['anexos'][0]['nome_arquivo'], PATHINFO_EXTENSION);
         if($ext != 'pdf'){
          //$caminho = "http://localhost/laercioimports/anexos/".$dados['anexos'][0]['nome_arquivo'];
          $caminho = "https://laercioimports.com.br/anexos/".$dados['anexos'][0]['nome_arquivo'];       
          $this->pdf->Image($caminho, 130, 70, 50);
         }
        }

        $this->pdf->Ln(10);

        $this->pdf->SetFont("Arial", "", 10); 
        $this->pdf->cell(100, 6, 'Cliente: '.utf8_decode($dados->nome), 0,1,'L');
        $this->pdf->cell(100, 6, 'Cpf: '.$dados->email, 0,1,'L');
        $this->pdf->cell(100, 6, 'Contato: '.$dados->telefone1, 0,1,'L');
        
        $this->pdf->Ln(2);        

     
        //dados do endereço 
        $this->pdf->SetFont("Arial", "B", 14);    
        $this->pdf->cell(100, 6, utf8_decode('Endereço para entrega'), 0,1,'l');
        $this->pdf->Ln(2);

        $this->pdf->SetFont("Arial", "", 10); 
        $this->pdf->cell(100, 6, 'Cep: '.$dados->endereco['cep'], 0,1,'l');
        $this->pdf->cell(100, 6, utf8_decode('Endereço: ').utf8_decode($dados->endereco['logradouro']), 0,1,'l');
        $this->pdf->cell(100, 6, 'Num: '.utf8_decode($dados->endereco['numero']), 0,1,'l');
        $this->pdf->cell(100, 6, 'Bairro: '.utf8_decode($dados->endereco['bairro']), 0,1,'l');
        $this->pdf->cell(100, 6, 'Estado: '.utf8_decode($dados->endereco['nome_estado']), 0,1,'l');
        $this->pdf->cell(100, 6, 'Cidade: '.utf8_decode($dados->endereco['municipio']), 0,1,'l');
        $this->pdf->cell(100, 6, 'Complemento: '.utf8_decode($dados->endereco['complemento']), 0,1,'l');

        $this->pdf->Ln(2);

        //itens do pedido
        $this->pdf->SetFont("Arial", "B", 14);    
        $this->pdf->cell(100, 6, utf8_decode('Itens do pedido'), 0,1,'l');
        $this->pdf->Ln(2);

        foreach($dados->itens as $item){
           $this->pdf->SetFont("Arial", "", 9); 
           $this->pdf->cell(100, 6, $item->qtde.'x - '.utf8_decode($item->item), 0,1,'L');
        }

        $this->pdf->Ln(2);

        //Observações
        $this->pdf->SetFont("Arial", "B", 14);    
        $this->pdf->cell(100, 6, utf8_decode('Observações'), 0,1,'l');
        $this->pdf->Ln(2);

        $this->pdf->cell(100, 6, utf8_decode($dados->observacao), 0,1,'l');

        $this->pdf->Ln(2);

        //Dados para pagamento
        $this->pdf->SetFont("Arial", "B", 14);    
        $this->pdf->cell(100, 6, utf8_decode('Dados do pagamento'), 0,1,'l');
        $this->pdf->Ln(2);

        $this->pdf->SetFont("Arial", "", 11); 
        $this->pdf->cell(100, 6, "Subtotal: ".utf8_decode(number_format($dados->total,2,',','.')), 0,1,'l');
        $this->pdf->cell(100, 6, "Frete: ".utf8_decode(number_format($dados->valor_entrega,2,',','.')), 0,1,'l');
        $this->pdf->cell(100, 6, "Total: ".number_format(($dados->total + $dados->valor_entrega),2,',','.'), 0,1,'l');        
    

    }//listarContasPagar

    public function saida(){
        //puxando o rodape    
        $this->footer(); 

        //criando a saida do pdf
        $data = str_replace('/','_', date('d/m/Y'));  
        $this->pdf->output('F', "relatorios/pedidos-".$data.".pdf");
        $this->pdf->output();
        echo BASE_URL.$arquivo;
    }//saida

}