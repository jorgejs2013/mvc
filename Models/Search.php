<?php
namespace Models;
use \Core\Model;
use PDO;

class Search extends Model{

public function buildGetFilterSql($filter){
$sqlFilter = array();

//pegando todos os campos dinamicos para a pesquisa 
foreach($filter as $ind=>$campo){
if(!empty($campo)){
	$sqlFilter[] .= $ind." LIKE :".$ind;
}
}

return $sqlFilter;
}//buildGetFilterSql

//& significa que tudo que fizer dentro tem ação fora
public function buildGetFilterBind($filter, &$sql){		

foreach($filter as $ind => $filtro){
  if($filtro !== ''){  
     $sql->bindValue(":".$ind, '%'.$filtro.'%');
  }		
}
}//buildGetFilterBind


public function getAllSearch($tabela, $filter = array(), $cond =  null){
$array = array();

$sqlfilter = $this->buildGetFilterSql($filter);

$sql = "SELECT * FROM {$tabela} ";
if($cond != null){
	$sql .= " $cond";
}

if(count($sqlfilter) >0){
	$sql .= " WHERE ".implode(' AND ', $sqlfilter);	
}

//$sql .= " ORDER BY id DESC";

$sql = $this->db->prepare($sql);
$this->buildGetFilterBind($filter, $sql);

$sql->execute();

if($sql->rowCount() >0){
	$array = $sql->fetchAll();
}

return $array;
}//getSearchAll

}