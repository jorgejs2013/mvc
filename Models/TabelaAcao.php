<?php
namespace models;
use core\Model;
use PDO;

class TabelaAcao extends model{

public function getListaPorTabela($id_tabela){
$sql = "SELECT * FROM usuario_tabela_acao ta, usuario_tabela t, usuario_acao a
WHERE ta.id_tabela = t.id_tabela AND ta.id_acao = a.id_acao AND ta.id_tabela = :id";
$sql = $this->db->prepare($sql);
$sql->bindValue(":id", $id_tabela);
$sql->execute();

return $sql->fetchAll();	
}//getListaPorTabela


public function getListaPorTabelaCliente($id_tabela){
$sql = "SELECT * FROM usuario_tabela_acao_cliente ta, usuario_tabela t, usuario_acao a
WHERE ta.id_tabela = t.id_tabela AND ta.id_acao = a.id_acao AND ta.id_tabela = :id";
$sql = $this->db->prepare($sql);
$sql->bindValue(":id", $id_tabela);
$sql->execute();

return $sql->fetchAll();	
}//getListaPortabelaCliente

}