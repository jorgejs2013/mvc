<?php
namespace Models;
use Core\Model;
use Models\Permissao;
use PDO;

/** Classe para Usuários */
class Users extends Model {

	/**Guarda os dados(informação) do usuário*/
	private $info;
	private $uid;	
	private $userName;
	private $slug;	
	private $userImage;
	private $isAdmin;
	private $getCompany;

public function isLogged(){

if(!empty($_SESSION['token'])){
	$token = $_SESSION['token'];
	$sql = "SELECT id, nome, slug_apelido, id_company,imagem FROM usuario WHERE loginhash = :token";
	$sql = $this->db->prepare($sql);
	$sql->bindValue(":token", $token);
	$sql->execute();

	if($sql->rowCount() >0){
       $p = new Permissao();

		$data = $sql->fetch();
		$this->uid = $data->id;
		$this->userName = $data->nome;
		$this->slug = $data->slug_apelido;
		$this->userImage = $data->imagem;	
		$this->getCompany = $data->id_company;
				
		return true;
	}
}

return false;

}//isLogged

public function validateLogin($email, $senha){	

$sql = "SELECT id, senha FROM usuario WHERE email = :email";

$sql = $this->db->prepare($sql);
$sql->bindValue(':email', $email);
$sql->execute();

if($sql->rowCount() >0){
 $data = $sql->fetch();

 if(password_verify($senha, $data->senha)){ 	
  $token = md5(time().rand(0,999).$data->id.time());

  $sql = "UPDATE usuario SET loginhash = :token WHERE id = :id";
  $sql = $this->db->prepare($sql);
  $sql->bindValue(":token", $token);
  $sql->bindValue(":id", $data->id);
  $sql->execute();

  $_SESSION['token'] = $token;
 return true; 
 }//verifica a senha 
}

return false;
}//validateLogin


public function getName(){
   return $this->userName;
}//getName

public function getSlug(){
   return $this->slug;
}//getName

public function getImg(){
	return $this->userImage;
}

public function isAdmin(){
	
	if($this->isAdmin == '1'){
     return true;
	}else{
		return false;
	}

}//isAdmin

public function getImage(){
return $this->userImage;
}//getImage


public function getId(){
 return $this->uid;
}//getId

public function getCompany(){
 return $this->getCompany;
}//getId


public function userExists($u){

$sql = "SELECT * FROM usuario WHERE email = :u";
$sql = $this->db->prepare($sql);
$sql->bindValue(":u",$u);
$sql->execute();

if($sql->rowCount() >0){
   return true;
}else{
   return false;
}

}//userExists

public function getInfo($id){
 $array = array();

$sql = $this->db->prepare("SELECT * FROM usuario WHERE email = :email ");
$sql->bindValue(':email', $id);
$sql->execute();

if($sql->rowCount()>0){
	$array = $sql->fetch();
}

  return $array;
}// function getInfo


}