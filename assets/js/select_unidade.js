$(document).on('change', '#grupo', function(){
let grupo = $(this).val();

$.ajax({
url:base_url+'ajax/changeGrupo',
method:'post',
data:{grupo:grupo},
dataType:'json',
beforeSend:function(){
$('#unidade').html('<option value="">Carregando...</option>');
},
success:function(res){
     
     $('#unidade').html('');
     $('.unidade').chosen('destroy'); 
	 
	 var template = '';
	 if(res.length >0){
	 res.forEach((un)=>{
       template += `<option value="${un.id_unidade}">${un.razao_social}</option>`;
	 });

	 $('#unidade').html(template);
	 $('.unidade').chosen({no_results_text: "Nenhum resultado encontrado!"});
	}else{
	$('#unidade').html('<option value="0">Nenhuma unidade cadastrada</option>');
	}
	 
}
}).done(function(){

$.ajax({
url:base_url+'ajax/getCentroDepartamento',
method:'post',
data:{grupo:grupo},
dataType:'json',
success:function(data){
var template_centro = '';
var template_departamento = '';

for(var i in data.centro){
	template_centro += `<option value="${data.centro[i].id_centrocusto}">${data.centro[i].nome}</option>`;
}	

for(var i in data.departamento){
	template_departamento += `<option value="${data.departamento[i].id_departamento}">${data.departamento[i].nome}</option>`;
}	

$('#centro').html(template_centro);
$('#departamento').html(template_departamento);
}	
});

});

});