$(document).ready(function(){

$('#summernote').summernote({
        placeholder: 'Ola escreva a descrição aqui',
        tabsize: 2,
        height: 200,
        callbacks:{
          onImageUpload:function(files){
            for(var i =0; i < files.length;i++){
                  upload(files[i]);
            }
          }
        },
        
        focus:true,
        lang: 'pt-BR',
        toolbar: [
          ['style', ['style']],
          ['font', ['bold', 'underline', 'clear']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['table', ['table']],
          ['insert', ['link']],
          ['view', ['fullscreen', 'codeview', 'help']]
        ]
      });


$('#summernote2').summernote({
placeholder: 'Ola escreva a descrição aqui',
tabsize: 2,
height: 200,
lang: 'pt-BR',
toolbar: [
          ['style', ['style']],
          ['font', ['bold', 'underline', 'clear']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['table', ['table']],
          ['insert', ['link']],
          ['view', ['fullscreen', 'codeview', 'help']]
        ]
});
$('#summernote3').summernote({
placeholder: 'Ola escreva a descrição aqui',
tabsize: 2,
height: 200,
lang: 'pt-BR',
toolbar: [
          ['style', ['style']],
          ['font', ['bold', 'underline', 'clear']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['table', ['table']],
          ['insert', ['link']],
          ['view', ['fullscreen', 'codeview', 'help']]
        ]
});
$('#summernote4').summernote({
placeholder: 'Ola escreva a descrição aqui',
tabsize: 2,
height: 200,
lang: 'pt-BR',
toolbar: [
          ['style', ['style']],
          ['font', ['bold', 'underline', 'clear']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['table', ['table']],
          ['insert', ['link']],
          ['view', ['fullscreen', 'codeview', 'help']]
        ]
});


});

function upload(file){
var datos = new FormData();
datos.append('file', file, file.name);
datos.append('rota', base_url);

$.ajax({
url:base_url+'ajax/uploadsummernote',
method:'POST',
data:datos,
contentType:false,
cache:false,
processData:false,
success:function(resposta){
  $('#summernote').summernote("insertImage", resposta);
},
error:function(jqXHR, textStatus, errorThrown){
  console.error(textStatus+" "+errorThrown);
}
});

}//upload