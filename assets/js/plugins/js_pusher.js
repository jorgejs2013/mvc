// Enable pusher logging - don't include this in production
Pusher.logToConsole = true;

var pusher = new Pusher('10b24c3f493874f821ad', {
  cluster: 'us2',
  forceTLS: true
});

var channel = pusher.subscribe('my-channel');
channel.bind('my-event', function(data) {
  //alert(JSON.stringify(data));
  $.ajax({
    url:base_url+'chat/listamsgs',
    dataType:'html',
    success:function(result){     

      setTimeout(function(){
        $('#messages').append(result);
        $('#messages').animate({scrollTop: $('#messages')[0].scrollHeight});
      },250);
      
    }
  });
});