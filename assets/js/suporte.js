$('.subirAnexos').change(function(){
var arquivos = this.files;

for(var i = 0;i< arquivos.length; i++){
	// Validar formatos de arquivos
 if(arquivos[i]['type'] != "image/jpeg" && 
 	arquivos[i]['type'] != "image/png" && 
 	arquivos[i]['type'] != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" &&
 	arquivos[i]['type'] != "application/vnd.ms-excel" &&
 	arquivos[i]['type'] != "application/vnd.openxmlformats-officedocument.wordprocessingml.document" &&
 	arquivos[i]['type'] != "application/msword" &&
 	arquivos[i]['type'] != "application/pdf"){
 
$(".subirAnexos").val("");

fncSweetAlert('error', 'O formato do arquivo nao esta correto, deve ser JPG, PNG, EXCEL, WORD ou PDF!','');

return;
 }else if(arquivos[i]['size'] > 32000000){
 	// validar tamanho dos arquivos
$(".subirAnexos").val("");

fncSweetAlert('error', 'Os arquivos nao podem ser maiores que 32MB!','');

}else{

multiplosArquivos(arquivos[i]);
	
}	

}//lado for
});	

var arquivosTemporarios = [];


function multiplosArquivos(arquivo){

var dadosArquivo = new FileReader;
dadosArquivo.readAsDataURL(arquivo);

$(dadosArquivo).on('load', function(event){

var rotaArquivo = event.target.result;

if(arquivo['type'] == 'image/jpeg' || arquivo['type'] == 'png'){
$('.mailbox_attachments').append(`
<li>
<span class="mailbox-attachment-icon has-img"><img src="${rotaArquivo}" alt="Attachment" /></span>	

<div class="maibox-attachment-info">
<a href="#" class="mailbox-attachment-name">
<i class="fas fa-camera"></i>${arquivo['name']}</a>	
<div class="maibox-attachment-size">
<span>${arquivo['size']}</span>
<button type="button" temporario class="btn quitarAnexo">
<i class="fas fa-times"></i>
</button>		
</div>
</div>
</li>`
);
}

if(arquivo['type'] == "application/vnd.openxmlformats-officedocument.spreadsheet.sheet" || arquivo['type'] == "application/vnd.ms-excel"){

$('.mailbox_attachments').append(`
<li>
<span class="mailbox-attachment-icon"><i class="far fa-file-excel"></i></span>	

<div class="maibox-attachment-info">
<a href="#" class="mailbox-attachment-name">
<i class="fas fa-paperclip"></i>${arquivo['name']}</a>	
<div class="maibox-attachment-size"><span>${arquivo['size']}</span>
<button type="button" temporario class="btn quitarAnexo">
<i class="fas fa-times"></i>
</button>	
</div>
</div>
</li>
`);
}

if(arquivo['type'] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" || arquivo['type'] == "application/msword"){

$('.mailbox_attachments').append(`
<li>
<span class="mailbox-attachment-icon"><i class="far fa-file-word"></i></span>	

<div class="maibox-attachment-info">
<a href="#" class="mailbox-attachment-name">
<i class="fas fa-paperclip"></i>${arquivo['name']}</a>	
<div class="maibox-attachment-size"><span>${arquivo['size']}</span>
<button type="button" temporario class="btn quitarAnexo">
<i class="fas fa-times"></i>
</button>	
</div>
</div>
</li>
`);
}

if(arquivo['type'] == "application/pdf"){

$('.mailbox_attachments').append(`
<li>
<span class="mailbox-attachment-icon"><i class="far fa-file-pdf"></i></span>	

<div class="maibox-attachment-info">
<a href="#" class="mailbox-attachment-name">
<i class="fas fa-paperclip"></i>${arquivo['name']}</a>	
<div class="maibox-attachment-size"><span>${arquivo['size']}</span>
<button type="button" temporario class="btn quitarAnexo">
<i class="fas fa-times"></i>
</button>	
</div>
</div>
</li>
`);
}

if(arquivosTemporarios.length !=0){
	arquivosTemporarios = JSON.parse($('.arquivosTemporarios').val());
}

arquivosTemporarios.push(rotaArquivo);
$('.arquivosTemporarios').val(JSON.stringify(arquivosTemporarios));

});
}//multiplos arquivos


$(document).on("click", ".quitarAnexo", function(){

var listaTemporarios = JSON.parse($('.arquivosTemporarios').val());
var quitarAnexo =  $('.quitarAnexo');

for(var i = 0;i < listaTemporarios.length;i++){

	$(quitarAnexo[i]).attr('temporario', listaTemporarios[i]);
	var quitarArquivo = $(this).attr('temporario');

  if(quitarArquivo == listaTemporarios[i]){
  	
     listaTemporarios.splice(i,1);
     $('.arquivosTemporarios').val(JSON.stringify(listaTemporarios));	
     $(this).parent().parent().parent().remove(); 
   }
}

});