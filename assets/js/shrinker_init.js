$(function(){
  $("table.shrink").tableShrinker({
    useZebra: true,
    transitionSpeed: 300,
    customToggle: ["<span>+</span>","<span>-</span>"],

  });
}); 

// shrink-xs: 480px
// shrink-sm: 768px
// shrink-md: 992px
// shrink-lg: 1200px
// shrink-xl: wider than 1200px
// shrinkable: makes text responsive