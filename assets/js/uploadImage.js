uploadImage()  
function uploadImage() {
      var button = $('.images .pic')
      var uploader = $('<input type="file" accept="image/*" />')
      var images = $('.images')
      
      button.on('click', function () {
        uploader.click()
      })
      
      uploader.on('change', function () {
          var reader = new FileReader()
          reader.onload = function(event) {
            images.prepend('<div class="img" style="background-image: url(\'' + event.target.result + '\');" rel="'+ event.target.result  +'"><span>remover</span><input type="hidden" name="imgs[]" value="'+event.target.result+'"/></div>')
          }
          reader.readAsDataURL(uploader[0].files[0])
  
       })
      
      images.on('click', '.img', function () {
        $(this).remove()
      })


      images.on('click', '.imgBD', function () {
        let id_img = $(this).data('image');

        $.ajax({
           url:base_url+'produtos/removeImg',
           method:'post',
           data:{id_img:id_img},
           dataType:'json',
           success:function(res){
            if(res == 'remove'){
              
            }
           }
        });
        $(this).remove();       
        
      })
    
    }