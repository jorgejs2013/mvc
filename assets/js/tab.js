var tabMenu = document.querySelectorAll('.js-tabmenu li');
var tabContent = document.querySelectorAll('.js-tabcontent section');
var activeClass = 'ativo';

//ativa a tab de acordo com o index da mesma
function activeTab(index){
tabContent.forEach((section)=>{
 section.classList.remove(activeClass);
});

const direcao = tabContent[index].dataset.anime;
tabContent[index].classList.add(activeClass, direcao);

//tabContent[index].classList.add(this.activeClass);	
}

function addTabNavEvent(){
 tabMenu.forEach((itemMenu, index) =>{
 itemMenu.addEventListener('click', () => activeTab(index));
});
}

activeTab(0);
addTabNavEvent();