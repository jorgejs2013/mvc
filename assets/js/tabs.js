$(function(){
  $('.tabConteudo').hide().filter(':first').show();
  
  $('.conteudo li[data-mostrar]').on('click',function(){
    $('.conteudo li[data-mostrar]').removeClass('ativa');
    $('.tabConteudo').hide();
    
    var mostrar =  $(this).data('mostrar');
    $(this).addClass('ativa');
    $('#'+mostrar).show();
  });
});