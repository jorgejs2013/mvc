// Função para formatar inputs
function fncFormatInputs(){
	if(window.history.replaceState){
		window.history.replaceState(null, null, window.location.href);
	}
}

//Função para Notie Alert
function fncNotie(type, text, time = null, posicao = null){
let tempo = 2;
if(time != null){
   tempo = time;	
}	

if(posicao == null){
	notie.alert({
      type:type,
      text:text,
      time:tempo
	});
}else{
	notie.alert({
      type:type,
      text:text,
      time:tempo,
      position:posicao
	});
}

}	

function fncSweetModal(id_item,titulo, preco, img){

$.ajax({
url:base_url+"ajax/pegaProduto",
method:'post',
data:{id_item:id_item},
dataType:'html',
success:function(res){

Swal.fire({
  title: '',
  icon: 'info',
  width: 'auto',
  html:res,    
  showCloseButton: true,
  showCancelButton: true,
  confirmButtonColor: '#066',
  cancelButtonColor:'#f00',
  focusConfirm: false,
  confirmButtonText:
    '<i class="fa fa-cart-plus"></i> Adicionar',
  confirmButtonAriaLabel: 'Adicionar',
  cancelButtonText:
    '<i class="fa fa-ban"></i> Cancelar',
  cancelButtonAriaLabel: 'Cancelar'
}).then((result) => {
   

  if(result.dismiss == 'cancel'){
   //Swal.fire('Changes are not saved', '', 'info')
  }else{
    //Swal.fire('Saved!', '', 'success');
   document.getElementById("btnModal").click();
  }
})


}
});

}//fncSweetModal


//Função para sweeAlert
function fncSweetAlert(type, text, url){
switch(type){
// Quando ocorre um erro
case 'error':
if(url == null){
Swal.fire({
 icon:'error',
 title:'Error',
 text:text
})
}else{
Swal.fire({
 icon:'error',
 title:'Error',
 text:text
}).then((result)=>{
	if(result.value){
		window.open(url, "_top");
	}
})
}
break;

// Quando ocorre um successo
case 'success':
if(url == null){
Swal.fire({
 icon:'success',
 title:'Success',
 text:text
})
}else{
Swal.fire({
 icon:'success',
 title:'Success',
 text:text
}).then((result)=>{
	if(result.value){
		window.open(url, "_top");
	}
})
}
break;

// Quando estmos precaregando
case 'loading':

Swal.fire({
allowOutsideClick:false,
icon:'info',
text:text
})
Swal.showLoading()
break;

case 'close':
Swal.close();
break;

case 'html':
Swal.fire({
  title: 'Preencha o formulário',
  icon: 'info',
  html:text,    
  showCloseButton: true,
  showCancelButton: true,
  focusConfirm: false,
  confirmButtonText:
    '<i class="fa fa-paper-plane"></i> Enviar',
  confirmButtonAriaLabel: 'Enviar',
  cancelButtonText:
    '<i class="fa fa-ban"></i> Cancelar',
  cancelButtonAriaLabel: 'Cancelar'
})
break;


// case 'confirm':
// return new Promise(resolve=>{
// Swal.fire({
// text:text,
// icon:'warning',
// showCancelButton:true,
// confirmButtonColor:"#3085d6",
// cancelButtonColor:'#d33',
// cancelButtonText:'Cancelar',
// confirmButtonText:'Sim, apagar!'
// }).then(function(result){
//   resolve(result.value);
// })

// });


case 'confirm':
Swal.fire({
  title: 'Deseja realmente apagar?',
  text: text,
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, apagar!',
  cancelButtonText:'Cancelar'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Removido!',
      'Seu registro foi removido.',
      'success'
    )
  }
})
break;

case 'modal':
Swal.fire({
  title: 'Aqui o meu modal dinamico',
  width: '90%',
  padding: '3em',
  background: '#fff',
  showCloseButton: true,
})
break;
}//comando switch


}//fncSweetAlert

function fncSweetRastreio(type,text, id){
Swal.fire({
html: `<form action='' method='post'>
<div class="group-form">

<label>Cadastre abaixo o código</label>
  <input id='codigo_rastreio' type='text' placeholder="Digite o código" />
</div>

</form>`,
title:"Código de rastreio",
confirmButtonText: `Salvar`,
cancelButtonText:`Cancelar`,
showCancelButton:true,
showCloseButton:true,
preConfirm:()=>{

var codigo = document.getElementById('codigo_rastreio').value;

if(codigo != '' && codigo.length == 13){
  return[ codigo ];
}else{  
  return [];
}

}
}).then((result)=>{

if(result.value.length == 0){
  Swal.fire('Preencha corretamente os números');
  return;
}

$.post(base_url+'ajax/rastreio',{rastreio:result.value[0],pedido:id}, function(data){
if(data == 'atualizado'){
   Swal.fire('Código atualizado com sucesso!');
  return;
}

},'json');

});

}//fnsSweetRastreio



function fncSweetMP(type, text, url){
Swal.fire({
  title: 'Preencha o formulário',
  icon: 'info',
  html:text,
  showDenyButton: true,
  showConfirmButton:false,
  showCancelButton: true,
  denyButtonText: `Cancelar`,  
});

const cardForm = mercadopago.cardForm({
         amount: 10,
         autoMount: true,
         form: {
             id: 'form-checkout',
             cardholderName: {
                 id: 'form-checkout__cardholderName',
                 placeholder: 'Nome completo',
             },
             cardNumber: {
                 id: 'form-checkout__cardNumber',
                 placeholder: 'Número do cartão',
             },
             CVV: {
                 id: 'form-checkout__CVV',
                 placeholder: 'Código de segurança',
             },
             installments: {
                 id: 'form-checkout__installments',
                 placeholder: 'Parcelas'
             },
             expirationMonth: {
                 id: 'form-checkout__expirationMonth',
                 placeholder: 'Mês de vencimento'
             },
             expirationYear: {
                 id: 'form-checkout__expirationYear',
                 placeholder: 'Ano de vencimento'
             },
             docType: {
                 id: 'form-checkout__docType',
                 placeholder: 'Tipo de documento'
             },
             docValue: {
                 id: 'form-checkout__docValue',
                 placeholder: 'N​ú​mero do documento​'
             },
             issuer: {
                 id: 'form-checkout__issuer',
                 placeholder: 'Banco emissor'
             }
         },
         callbacks: {
            onFormMounted: function(error) {
                if (error) return console.log('Callback handling error ', error);
            },
            onCardTokenReceived: function(error, token) {
                if (error) return console.log('Callback handling error ', error);

                const formData = cardForm.getCardFormData()
                console.log('form Data: ', formData);

                // post data to your backend
                //quando a promesssa é executada
                formData.then(resp=>{

                   $.ajax({
                     url:base_url+'mp/processar',
                     method:'POST',
                     data:{
                          transactionAmount:10,
                          identificationNumber:resp.identificationNumber,
                          identificationType:resp.identificationType,
                          installments:resp.installments,
                          issuerId:resp.issuerId,
                          merchantAccountId:resp.merchantAccountId,
                          paymentMethodId:resp.paymentMethodId,
                          processingMode:resp.processingMode,
                          token:resp.token 
                        },
                     success:function(resposta){
                      console.log(resposta);
                     }
                   });  
                });
            },
        }
     })

     document.getElementById('form-checkout').addEventListener('submit', function(e) {
         e.preventDefault();
         cardForm.createCardToken()
     })
}//fncSweetMP




function limpaUrl(texto){
texto = texto.toLowerCase();
texto = texto.replace(/[á]/g,'a');  
texto = texto.replace(/[é]/g,'e');
texto = texto.replace(/[í]/g,'i');
texto = texto.replace(/[ó]/g,'o');
texto = texto.replace(/[ú]/g,'u');
texto = texto.replace(/[ñ]/g,'n');
texto = texto.replace(/ /g,'-');

return texto;
}


$(document).on('click','.fncSweetDel', function(e){
e.preventDefault();
const href = $(this).attr('href');

Swal.fire({
  title: 'Deseja realmente apagar?',
  text: 'Apagar registro',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, apagar!',
  cancelButtonText:'Cancelar'
}).then((result) => {

  if (result) {
    document.location.href = href;    
  }
})

})

//como utilizar
// $(document).on('keyup', '.inputRota', function(){
// $(this).val();
// limpaUrl($(this).val()) 
// })

 //fncSweetAlert("html", formMP, null);
// fncSweetAlert("modal", 'teste', null);