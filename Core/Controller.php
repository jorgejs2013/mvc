<?php
namespace Core;

class Controller {

	protected $db;		
	
	public function loadView($viewName, $viewData = array()) {
		extract($viewData, EXTR_PREFIX_SAME, '_');		
		include 'Views/'.$viewName.'.php';
	}

	public function loadTemplate($viewName, $viewData = array()) {
		include 'Views/template.php';
	}

	public function loadViewInTemplate($viewName, $viewData) {	
		extract($viewData, EXTR_PREFIX_SAME, '_');		
		include 'Views/'.$viewName.'.php';
	}	

	public function loadLibrary($lib, $viewData){
		if(file_exists('libraries/'. $lib.'.php')){
		  extract($viewData, EXTR_PREFIX_SAME, '_');
          include 'libraries/'. $lib.'.php';
		}		
	}//loadLibrary

public function Slug($Name){
$this->Format = array();
$this->Format['a'] = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿRr"!@#$%&*()_-+={[}]/?;:.,\\\'<>°ºª';  
$this->Format['b'] = 'aaaaaaaceeeeiiiidnoooooouuuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr                                 ';
$this->Data = strtr(utf8_decode($Name), utf8_decode($this->Format['a']), utf8_decode($this->Format['b']));  
$this->Data = strip_tags(trim($this->Data));

$this->Data = str_replace(' ','-', $this->Data);
$this->Data = str_replace(array('-----', '----', '---','--'),'-', $this->Data);

return strtolower(utf8_encode($this->Data));
}//fim function name

public function limpaCampo($campo = null){

if($campo != null){

 if(isset($campo) && !empty($campo)){
  $novoCampo = addslashes(trim($campo));
  return $novoCampo;
}
}else{
  return null;
}
}//limpaCampo



public function validateUsername($u){
// ^ = comeca uma coisa ate o fim
if(preg_match('/^[a-z0-9]+$/', $u)):
  return true;
else:
  return false;
endif;
}//validateUsername


public function flashMessage($sessao){ 

if(isset($sessao) && !empty($sessao)){
 $msg = $sessao;
 unset($_SESSION['msg']);
  return $msg;
}else{
  return NULL;
} 
    
}//flashMessage


public function pesquisaPermissao($array, $tabela, $acao){ 
 if(is_array($array)){

  foreach($array as $key => $permissoes){ 
  
    if($permissoes['tabela'] == $tabela && $permissoes['acao'] == $acao){
    return true;
    }
    continue;
    
  } 
  return false;
 }//verifica se é array
}//pesquisaPermissao  

public function limpaString($string){
$arrray = array();
$arr1 = '/-_.,';
$arr2 = '     ';

$data = strtr(utf8_decode($string), utf8_decode($arr1), utf8_decode($arr2));
$data = strip_tags($data); 
$data = str_replace(' ', '', $data);
$data = str_replace(array('-----', '----', '---','--'),'-', $data);

return strtolower(utf8_encode($data));
}//limpaString

	public function debug($array){
     echo "<pre>";
     print_r($array);
     exit;
	}//debug

	public function redirect($target){
       return header("Location:{$target}");
     }//redirect

    public function back(){
	$previous = "javascript:history.go(-1)";
	if(isset($_SERVER['HTTP_REFERER'])){
		$previous = $_SERVER['HTTP_REFERER'];
	}

	return header("Location: {$previous}");
 
    }//back


    public function converterMoeda($valor){     
    	$newValor = number_format( (float) $valor,2,',','.');  
      
      return $newValor;   
     }//converterMoeda


     public function converterData($data, $pattern = '-'){     
       $novaData = explode($pattern, $data);
       $novaData = $novaData[2].'-'.$novaData[1].'-'.$novaData[0];
       return $novaData;
     }//converterData


     public function valor($valor) {
       $verificaPonto = ".";
       if(strpos("[".$valor."]", "$verificaPonto")):      
           $valor = str_replace('.','', $valor);
           $valor = str_replace(',','.', $valor);
           else:           	
             $valor = str_replace(',','.', $valor);   
       endif;

       return $valor;
    }//valor

	public function gerarNome($total_caracteres){

     $caracteres = 'ABCDEFGHIJKLMNOPQRSTUWXYZ';
     $caracteres .= 'abcdefghijklmnopqrstuwxyz';
     $caracteres .= '0123456789';
     $max = strlen($caracteres)-1;
     $senha = null;
     for($i=0; $i < $total_caracteres; $i++){
        $senha .= $caracteres{mt_rand(0, $max)};
    }
    return $senha;
}//function gerarNome


public function geraCodigo($tamanho = 10, $maiusculas = false, $numeros = true, $simbolos = false)
{
// Caracteres de cada tipo
$lmin = 'abcdefghijklmnopqrstuvwxyz';
$lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
$num = '1234567890';
$simb = '!@#$%*-';
// Variáveis internas
$retorno = '';
$caracteres = '';
// Agrupamos todos os caracteres que poderão ser utilizados
//descomente a linha de baixao para gerar letras misturadas
//$caracteres .= $lmin; 
if ($maiusculas) $caracteres .= $lmai;
if ($numeros) $caracteres .= $num;
if ($simbolos) $caracteres .= $simb;
// Calculamos o total de caracteres possíveis
$len = strlen($caracteres);
for ($n = 1; $n <= $tamanho; $n++) {
// Criamos um número aleatório de 1 até $len para pegar um dos caracteres
$rand = mt_rand(1, $len);
// Concatenamos um dos caracteres na variável $retorno
$retorno .= $caracteres[$rand-1];
}
return $retorno;
}//geraSenha

public function imagemValida($imagem){
if($imagem['type'] == 'image/jpeg' ||
    $imagem['type'] == 'imagem/jpg' ||
    $imagem['type'] == 'imagem/png' ||
    $imagem['type'] == 'imagem/gif'){

$tamanho = intval($imagem['size']/1024);

if($tamanho < 2097152){
 return true;
}else{
  return false;
}   

}else{
   return false;
 }
}//imagemValida

public function gravarImagem($imagem, $dimensoes, $path){
//criamos a pasta caso ela n exista ainda
if(!file_exists($path)){
  mkdir($path, 0755);
}

$image = $imagem['tmp_name'];
$ext = pathinfo($imagem['name'], PATHINFO_EXTENSION);
//criamos o nome da imagem
$aleatorio = md5(mt_rand(100,999));
$rota = $path.'/'.$aleatorio.'.'.$ext;

list($largura_original, $altura_original) = getimagesize($image);
$ratio = $largura_original / $altura_original;
// $novaLagura = 680;
// $novaAltura = 400;
$largura = $dimensoes[0];
$altura = $dimensoes[1];

if($largura / $altura > $ratio){
  $largura = $altura* $ratio;
}else{
  $altura = $largura / $ratio;
}

//verificando os tipos de imagens
switch($ext){
case 'jpeg':
case 'jpg':
$origem = imagecreatefromjpeg($image);
$destino = imagecreatetruecolor($largura, $altura);
imagecopyresized($destino, $origem, 0, 0, 0, 0, $largura, $altura, $largura_original, $altura_original);
imagejpeg($destino, $rota); 

imagedestroy($destino);

return $rota;
break;

case 'png':
$origem = imagecreatefrompng($image);
$destino = imagecreatetruecolor($largura, $altura);
imagealphablending($destino, FALSE);
imagesavealpha($destino, TRUE);
imagecopyresampled($destino, $origem, 0, 0, 0, 0, $largura, $altura, $largura_original, $altura_original);
imagepng($destino, $rota);

imagedestroy($destino);
return $rota;
break;

case 'gif':
$nomeImagem = md5(mt_rand(100,999).time()).'.gif';
move_uploaded_file($image,  $dir.$nomeImagem);
return $nomeImagem;   
break;
}

}//gravarImagem


public function gravaAnexos($anexo){
$retorno = '';

if($anexo !== ""){

$diretorio = 'medias/';

$anexos_array = array();
$anexos = json_decode($anexo, true);

foreach($anexos as $key=>$value){

$separarAnexo = explode(';', $value);
$separarBase64 = explode(",", $separarAnexo[1]);

switch($separarAnexo[0]){
 case 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
 case 'data:application/vnd.ms-excel':

 $aleatorio = mt_rand(100,999);
 $rota = $diretorio."/".$aleatorio.".xlsx";
 $arquivo = base64_decode($separarBase64[1]);
 file_put_contents($rota, $arquivo);

 array_push($anexos_array, $rota);

 break;

 case 'data:application/vnd.openxmlformats-officedocument.wordprocessingml.document':
 case 'data:application/msword':
 $aleatorio = mt_rand(100,999);
 $rota = $diretorio."/".$aleatorio.".docx";
 $arquivo = base64_decode($separarBase64[1]);
 file_put_contents($rota, $arquivo);

 array_push($anexos_array, $rota);
 break;

 case 'data:application/pdf':
 $aleatorio = mt_rand(100,999);
 $rota = $diretorio."/".$aleatorio.".pdf";
  $arquivo = base64_decode($separarBase64[1]);
 file_put_contents($rota, $arquivo);

 array_push($anexos_array, $rota);
 break;

 case 'data:image/jpeg':
 case 'data:image/jpg':
 $aleatorio = mt_rand(100,999);
 $rota = $diretorio."/".$aleatorio.".jpeg";
  $arquivo = base64_decode($separarBase64[1]);
 file_put_contents($rota, $arquivo);

 array_push($anexos_array, $rota);
 break;

 case 'data:image/png':
 $aleatorio = mt_rand(100,999);
 $rota = $diretorio."/".$aleatorio.".png";
  $arquivo = base64_decode($separarBase64[1]);
 file_put_contents($rota, $arquivo);

 array_push($anexos_array, $rota);
 break;

 default:
 $retorno = array(
'error'=>1,
'msg'=>'Não se permitem formatos diferentes a JPG, PNG, Word, Excel, Pdf',
'files'=>NULL
 );

 return $retorno;
 break;

}//switch
}//finaliza o foreach

$retorno = array(
'error'=>0,
'msg'=>"Upload feito com sucesso",
'files'=>json_encode($anexos_array)
);

return $retorno;
}
}//gravaAnexos
}