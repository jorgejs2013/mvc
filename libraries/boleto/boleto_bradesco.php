<?php
$online = 'sim';

if($online == 'sim'){
try{
$dsn = "mysql:dbname=unixsist_boletosmil;host=bdhost0043.servidorwebfacil.com";
$dbuser = "unixsist_userbol";
$dbpass = "#boletosmil#";
$pdo = new PDO($dsn, $dbuser, $dbpass);
}catch(PDOException $e){
	die($e->getMessage());		
}

}else{
try{
$dsn = "mysql:dbname=dinamica;host=localhost";
$dbuser = "root";
$dbpass = "";
$pdo = new PDO($dsn, $dbuser, $dbpass);
}catch(PDOException $e){
	die($e->getMessage());		
}	
}

if(isset($id_boleto)){
    $idf = $id_boleto;
}else{
	$idf = 0;
}

//pegando o boleto para imprimir
$qr = "SELECT * FROM fatura_parcelas WHERE id_fatura_parcela = :id_fatura";
$qr = $pdo->prepare($qr);
$qr->execute([':id_fatura'=>$idf]);
$fat  = $qr->fetch(PDO::FETCH_OBJ)->id_fatura;


//pegando agora a fatura
$pesq = $pdo->prepare("SELECT * FROM faturas WHERE id_fatura  = :id_fatura ");
$pesq->bindValue(":id_fatura", $fat);
$pesq->execute();

if($pesq->rowCount() <= 0){
	echo "nao tem fatura";
	exit;
	header("Location:".BASE_URL."faturas");
	exit;
}

//Pegando dados da empresa
$sql = $pdo->prepare("SELECT * FROM empresa e, cidade c, estado es
WHERE e.id_cidade = c.id_cidade AND
e.id_estado = es.id_estado AND
e.id_empresa = '1'");
$sql->execute();
$empresa = $sql->fetch(PDO::FETCH_OBJ);

//Pegando dados da fatura
$sql2 = $pdo->prepare("SELECT * FROM faturas f, fatura_parcelas p, contato c, contas co, cidade ci, estado es WHERE 
f.id_cliente = c.id_contato AND
f.id_conta = co.id AND
c.id_cidade = ci.id_cidade AND 
c.id_estado = es.id_estado AND
f.id_fatura = p.id_fatura
AND p.id_fatura_parcela = :id");
$sql2->bindValue(":id", $id_boleto);
$sql2->execute();
$fatura = $sql2->fetch(PDO::FETCH_OBJ);

//informar ao dashboard que o cliente ja clicou em imprimir ou visualziar o boleto
$visto = $pdo->prepare("UPDATE fatura_parcelas SET visto = 'S', data_visto = :data WHERE id_fatura_parcela = :id");
$visto->bindValue(':data', date('Y-m-d'));
$visto->bindValue(':id', $idf);
$visto->execute();

//Convertendo os campos de data para o padrao correto
$dc = explode("-", $fatura->data_cad);
$datac = $dc[2]."/".$dc[1]."/".$dc[0];

$dv = explode("-", $fatura->venc_parcela);
$datav = $dv[2]."/".$dv[1]."/".$dv[0];


// DADOS DO BOLETO PARA O SEU CLIENTE
$dias_de_prazo_para_pagamento = 5;
$taxa_boleto = 0;
$data_venc = $datav;//date("d/m/Y", time() + ($dias_de_prazo_para_pagamento * 86400));  // Prazo de X dias OU informe data: "13/04/2006";
$valor_cobrado = $fatura->valor_parcela;//"2950,00"; // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
$valor_cobrado = str_replace(",", ".",$valor_cobrado);
$valor_boleto = number_format($valor_cobrado+$taxa_boleto, 2, ',', '');


$dadosboleto["nosso_numero"] = $fatura->id_fatura_parcela;//"75896452";  // Nosso numero sem o DV - REGRA: M�ximo de 11 caracteres!
$dadosboleto["numero_documento"] = $fatura->id_fatura;//$dadosboleto["nosso_numero"];	// Num do pedido ou do documento = Nosso numero
$dadosboleto["data_vencimento"] = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
$dadosboleto["data_documento"] = $datac; //date("d/m/Y"); // Data de emiss�o do Boleto
$dadosboleto["data_processamento"] = date("d/m/Y"); // Data de processamento do boleto (opcional)
$dadosboleto["valor_boleto"] = $valor_boleto; 	// Valor do Boleto - REGRA: Com v�rgula e sempre com duas casas depois da virgula

//DADOS DA EMPRESA
$dadosboleto['cnpj'] = $empresa->cnpj;
$dadosboleto['logradouro'] = $empresa->logradouro;
$dadosboleto['numero_empr']  = $empresa->numero;
$dadosboleto['cidade_estado_empresa'] = $empresa->nome_cidade.'/'.$empresa->nome_estado;

// DADOS DO SEU CLIENTE
$dadosboleto["sacado"] = $fatura->nome_resp.". CPF/CNPJ:".$fatura->cpf;//"Nome do seu Cliente";
$dadosboleto["endereco1"] = $fatura->logradouro. ". ".$fatura->bairro;//"Endere�o do seu Cliente";
$dadosboleto["endereco2"] = $fatura->nome_cidade. ' - '.$fatura->nome_estado.' CEP '.$fatura->cep; //"Cidade - Estado -  CEP: 00000-000";

// INFORMACOES PARA O CLIENTE
$dadosboleto["demonstrativo1"] = "Pagamento ref. a ".$fatura->descricao;
$dadosboleto["demonstrativo2"] = $empresa->nome_fantasia;//"Mensalidade referente ao m�s de Outubro<br>Taxa banc�ria - R$ ".number_format($taxa_boleto, 2, ',', '');
$dadosboleto["demonstrativo3"] = $empresa->logradouro.". ".$empresa->bairro.". ".$empresa->nome_cidade.". CEP:".$empresa->cep;// linha opcional
$dadosboleto["instrucoes1"] = $fatura->instrucao1;
$dadosboleto["instrucoes2"] = $fatura->instrucao2;
$dadosboleto["instrucoes3"] = $fatura->instrucao3;
$dadosboleto["instrucoes4"] = "";

// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = "001";
$dadosboleto["valor_unitario"] = $valor_boleto;
$dadosboleto["aceite"] = "N";
$dadosboleto["especie"] = "R$";
$dadosboleto["especie_doc"] = "DS";

// ---------------------- DADOS FIXOS DE CONFIGURA��O DO SEU BOLETO --------------- //
// DADOS DA SUA CONTA - Bradesco
$dadosboleto["agencia"] = $fatura->agencia;//"1100"; // Num da agencia, sem digito
$dadosboleto["agencia_dv"] = $fatura->ag_dv;//"0"; // Digito do Num da agencia
$dadosboleto["conta"] = $fatura->conta;//"0102003"; 	// Num da conta, sem digito
$dadosboleto["conta_dv"] = $fatura->conta_dv;//"4"; 	// Digito do Num da conta

// DADOS PERSONALIZADOS - Bradesco
$dadosboleto["conta_cedente"] = $fatura->conta;// "0102003"; // ContaCedente do Cliente, sem digito (Somente N�meros)
$dadosboleto["conta_cedente_dv"] = $fatura->conta_dv;//"4"; // Digito da ContaCedente do Cliente
$dadosboleto["carteira"] = $fatura->carteira;  // C�digo da Carteira: pode ser 06 ou 03

// SEUS DADOS
$dadosboleto["identificacao"] = $fatura->descricao;//"BoletoPhp - C�digo Aberto de Sistema de Boletos";
$dadosboleto["cpf_cnpj"] = $empresa->cnpj;//"";
$dadosboleto["endereco"] = $empresa->logradouro;//"Coloque o endere�o da sua empresa aqui";
$dadosboleto["cidade_uf"] = $empresa->nome_cidade. ' - '.$empresa->nome_estado;;//"Cidade / Estado";
$dadosboleto["cedente"] = $empresa->nome_fantasia;//"Coloque a Raz�o Social da sua empresa aqui";

// N�O ALTERAR!
include("include/funcoes_bradesco.php");
include("include/layout_bradesco.php");
?>