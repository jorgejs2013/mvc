<?php
require 'environment.php';
global $config;
date_default_timezone_set('America/Bahia');


$config = array();
if(ENVIRONMENT != 'development') {
	define("BASE_URL", "http://localhost/mvc/");    
    $config['dbname'] = 'prime';	
    $config['host'] =  'localhost';	
    $config['dbuser'] = 'root';	
    $config['dbpass'] = '';
    } else {
	define("BASE_URL", "http://localhost/mvc/");   
    $config['dbname'] = 'prime';    
    $config['host'] =  'localhost'; 
    $config['dbuser'] = 'root'; 
    $config['dbpass'] = '';
}

//Conectando ao banco de dados
try{
$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
$db = new PDO("mysql:dbname=".$config['dbname'].";host=".$config['host'], $config['dbuser'], $config['dbpass'], $options);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);              
}catch(PDOException $e){
 echo "erro: ".$e->getMessage()."<br> - Erro na linha ".$e->getLine();        
}